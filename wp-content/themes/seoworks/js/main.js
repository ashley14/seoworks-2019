// document.addEventListener('touchstart', handler, {capture: true});

jQuery(function($){
  $(".carousel-clients").owlCarousel(
    {
      loop:true,
      margin:10,
      nav: false,
      dots: true,
      margin:30,
      autoplay: true,
      autoplayTimeout: 5000,
      slideBy: 'page',
      responsive:{
          0:{
              items:2
          },
          300:{
              items:3
          },
          600:{
              items:4
          },
          1000:{
              items:5
          }
      }
    }
  );

    $(".carousel-awards").owlCarousel(
      {
        loop: true,
        margin:10,
        nav: false,
        dots: true,
        margin:30,
        autoplay: true,
        autoplayTimeout: 5000,
        responsive:{
          0:{
              items:2
          },
          300:{
              items:3
          },
          600:{
              items:4
          },
          1000:{
              items:5
          }
        }
      }
    );

    // carousel Reviews

    $(".carousel-reviews").owlCarousel(
      {
        loop:true,
        items:1,
        // margin:10,
        nav: true,
        dots: false,
        margin:30,
        autoHeight:false,
        autoplay: true,
        autoplayTimeout: 5000
      }
    );

    $(".carousel-case_studies").owlCarousel(
      {
        loop: true,
        margin:10,
        nav: false,
        dots: true,
        margin:30,
        autoplay: true,
        autoplayTimeout: 5000,
        responsive:{
          0:{
              items:2
          },
          300:{
              items:3
          },
          600:{
              items:4
          },
          1000:{
              items:5
          }
        }
      }
    );

    $(window).scroll(function() {
      // find the id with class 'sticky' and remove it
      // $("#header").removeClass("sticky");
      // get the amount the window has scrolled
      var scroll = $(window).scrollTop();
      // add the 'sticky' class to the correct id based on the scroll amount
      if (scroll >= 550) {
          $("#header").addClass("sticky");
      }
      if (scroll > 550) {
          $("body.case-studies-template-single-case-studies-parallax #header").removeClass("sticky");
          $("body.page-template-template-portfolio #header").removeClass("sticky");
      }
      if (scroll <= 550) {
          $("#header").removeClass("sticky");
      }
      // if (parallaxPage && (scroll >= 550)) {
      //     $("#header").removeClass("sticky");
      // }
    });

    $('.menu-item-has-children').append('<span class="sub-menu__arrow"></span>');
    $('.sub-menu__arrow').click(function() {
      $('ul.sub-menu').toggle('slow');
    });

    var url = window.location.href;
    $('.filter a[href="'+url+'"]').addClass('active');

    $('a').on('click', function() {
      var id = $(this).attr('href');
      if ($(window).width() < 991) {
      $('html, body').animate({scrollTop: $(id).offset().top - 160}); // scroll to the id of the clicked link without the stiky menu hadding content
    } else {
      $('html, body').animate({scrollTop: $(id).offset().top - 240}); // scroll to the id of the clicked link without the stiky menu hadding content
    }
    });

    // BUtton back to top
    var btn = $('#back-top-top');
    $(window).scroll(function() {
      if ($(window).scrollTop() > 400) {
        btn.addClass('show-top');
      } else {
        btn.removeClass('show-top');
      }
    });

    btn.on('click', function(e) {
      e.preventDefault();
      $('html, body').animate({scrollTop:0}, '300');
    });

	// Script for a smooth scrolling. Will affect the links which have # in the href <a href="#">Link Text</a>
  $("a[href|='#']").on('click', function(event) {
    if (this.hash !== "") {
      event.preventDefault();
      var hash = this.hash;
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 1000, function(){
        window.location.hash = hash;
      });
    }
	});


});
