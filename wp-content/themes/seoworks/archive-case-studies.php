<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package SeoWorks
 */

get_header();
?>

<div class="archive">
  <div class="header-content divider-bottom-left" style="background-image: url(<?php the_field('casestidues_archive_background_image', 'option'); ?>);">
  	<div class="container">
  		<div class="row">
  			<div class="col-12">
  				<div class="title">
              <h1><?php the_field('casestidues_archive_title', 'option'); ?></h1>
  				</div>
  				<p>
            <?php the_field('casestidues_archive_description', 'option'); ?>
  				</p>
  			</div>
  		</div>
  	</div>
  </div>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
      <?php
        // your taxonomy name
        $tax_sector = 'sector';
        $tax_campaign = 'campaign';
        // get the terms of taxonomy
        $terms_sector = get_terms( $tax_sector, $args = array(
          'hide_empty' => true, // do not hide empty terms
        ));
        $tax_campaign = get_terms( $tax_campaign, $args = array(
          'hide_empty' => true, // do not hide empty terms
        ));
      ?>
      <div class="container">
        <div class="row">
          <div class="col-12">

            <div class="case-studies_filter case-studies_filter-sector">
              <!-- <div class="title">
                <h4>Sectors</h4>
              </div> -->
              <div class="filter">
                <?php
                  echo '<button type="button" data-filter="all">All</button>';
                  if ( ! empty( $terms_sector ) && ! is_wp_error( $terms_sector ) ){
                      // loop through all terms
                      foreach( $terms_sector as $term ) {
                          // Get the term link
                          $term_link = get_term_link( $term );
                          if( $term->count > 0 )
                             // display term name
                            // echo '<button type="button" data-filter="' . '.' . $term->slug . '"> ' . $term->name . '</button>';
                            echo '<a href="'. get_home_url() .'/sector/'. $term->slug .'"> ' . $term->name . '</a>';
                      }
                  }
                  ?>
              </div>

            </div>
          </div>

          <div class="col-12">

            <div class="case-studies_filter case-studies_filter-campaign">
              <!-- <div class="title">
                <h4>Campaigns</h4>
              </div> -->
              <div class="filter">
                <?php
                // echo '<button type="button" data-filter="all">All</button>';
                if ( ! empty( $tax_campaign ) && ! is_wp_error( $tax_campaign ) ){
                    // loop through all terms
                    foreach( $tax_campaign as $term ) {
                        // Get the term link
                        $term_link = get_term_link( $term );
                        if( $term->count > 0 )
                           // display term name
                          // echo '<button type="button" data-filter="' . '.' . $term->slug . '"> ' . $term->name . '</button>';
                          // echo '<button type="button" data-filter="' . '.' . $term->slug . '"> ' . $term->name . '</button>';
                          echo '<a href="'. get_home_url() .'/campaign/'. $term->slug .'"> ' . $term->name . '</a>';
                    }
                }
                ?>
              </div>
            </div>
          </div>

        </div>
      </div>

      <div class="container">
        <div class="row">
          <div class="col-12">
            <?php echo do_shortcode('[clients-slider] '); ?>
          </div>
        </div>
      </div>

      <section>
  			<div class="sections">
  				<div class="container">
  					<div class="row sectors">
  						<?php
              // Query Arguments
          		$args = array(
          		'post_type' => array('case-studies'),
          		'posts_per_page' => -1,
          		'order' => 'DESC',
          		);

              // The Query
          		$case_studies = new WP_Query( $args );

          		// The Loop
          		if ( $case_studies->have_posts() ) {
          		while ( $case_studies->have_posts() ) {
          			$case_studies->the_post();
          			$getslugid = wp_get_post_terms( $post->ID, array('campaign', 'sector') );
          			$slugs = implode(' ',wp_list_pluck($getslugid,'slug'));
          		?>

                <article class="col-sm-6 col-xl-4 mix <?php echo $slugs; ?>">
                  <div class="card">
                    <div class="card-image">
                      <figure>
                        <a href="<?php the_permalink(); ?>">
                          <?php if (class_exists('MultiPostThumbnails')) :
                            MultiPostThumbnails::the_post_thumbnail(
                                get_post_type(),
                                'second-image-casestudy',
                                add_image_size('thumb-blog')
                            );
                          endif; ?>
                        </a>
                      </figure>
                    </div>
                    <div class="card-body">
                      <h5 class="card-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                      <div class="card-text"><?php the_excerpt(); ?></div>
                      <a href="<?php the_permalink(); ?>" class="btn btn-readmore_blog">Read More</a>
                    </div>
                  </div>
                </article>

              <?php }
            		} else {
            		// no posts found
            		}
            		/* Restore original Post Data */
            		wp_reset_postdata();
          		?>

  					</div>
  				</div>
  			</div>
      </section>



		</main><!-- #main -->
	</div><!-- #primary -->
</div>

<?php
// get_sidebar();
get_footer();?>
<script type="text/javascript">
  // MixItUp Homepage portfolio
  var mixer = mixitup('.sectors');
</script>
