<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package SeoWorks
 */

get_header();
$featured_img = get_the_post_thumbnail_url(get_the_ID(),'full');
$featured_default_img = get_template_directory_uri(). '/assets/images/SeoWorks-Generic-Header.jpg';
?>
<div class="blog">
	<div class="header-content divider-bottom-left" style="background-image:url('<?php if (empty($featured_img)) {echo $featured_default_img; } else { echo $featured_img;} ?>')">
		<div class="container">
			<div class="row">

			</div>
		</div>
	</div>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="content">
        <section class="post">
          <div class="container">
            <div class="row">
              <div class="col-md-8">

								<?php
								while ( have_posts() ) :
									the_post();

									get_template_part( 'template-parts/content', get_post_type() );

									// the_post_navigation();

									// // If comments are open or we have at least one comment, load up the comment template.
									// if ( comments_open() || get_comments_number() ) :
									// 	comments_template();
									// endif;

								endwhile; // End of the loop.
								?>
						</div>

						<div class="col-md-4">
							<?php get_sidebar(); ?>
						</div>
					</div>
				</div>
			</section>

		</main><!-- #main -->
	</div><!-- #primary -->
</div>

<div id="pagination-single" class="pagination-single">

	<div class="container">
		<div class="row">
			<!-- <div id="pagination" class="pagination"> -->
				<div class="col-sm-12 col-md-4 align-middle pagination-previous">
					<?php previous_post_link('%link', __( 'Prev. Post', 'seoworks.co.uk' ), true);?>
				</div>
				<div class="col-sm-12 col-md-4 align-middle pagination-all">
					<a href="/blog">All Blogs</a>
				</div>
				<div class="col-sm-12 col-md-4 align-middle pagination-next">
					<?php next_post_link( '%link', 'Next Post', true ); ?>
				</div>
			<!-- </div> -->
		</div>
	</div>

</div>

<?php
get_footer();
?>
