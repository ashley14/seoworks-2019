<?php
/**
 ***** Template Name: Home Page
 *
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @version 1.0
 */

get_header(); ?>
<?php
  // get_template_part( 'template-parts/homepage/home', 'header');

  get_template_part( 'template-parts/homepage/home', 'clients');

  get_template_part( 'template-parts/homepage/home', 'reviews');

  get_template_part( 'template-parts/homepage/home', 'services');

  get_template_part( 'template-parts/homepage/home', 'sectors');

  get_template_part( 'template-parts/homepage/home', 'sectors-portfolio');

  get_template_part( 'template-parts/homepage/home', 'awards');

  get_template_part( 'template-parts/homepage/home', 'blog');

?>



<?php get_footer(); ?>
