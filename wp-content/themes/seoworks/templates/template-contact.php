<?php
/**
 ***** Template Name: Contact
 *
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @version 1.0
 */

get_header();
$featured_img = get_the_post_thumbnail_url(get_the_ID(),'full');
$featured_default_img = get_template_directory_uri(). '/assets/images/SeoWorks-Generic-Header.jpg' ;
?>
<div class="header-content divider-bottom-left" style="background-image:url(<?php if (empty($featured_img)) {echo $featured_default_img; } else { echo $featured_img;} ?>)">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <?php
          while ( have_posts() ) :
            the_post();
        ?>
        <div class="title">
            <h1><?php the_title(); ?></h1>
        </div>

        <p><?php the_content(); ?></p>
        <?php
          endwhile; // End of the loop.
        ?>
      </div>
    </div>
  </div>
</div>

<div id="primary" class="content-area">
  <main id="main" class="site-main">
    <div class="content">
      <section class="post">
        <div class="container">
          <div class="row">
            <div class="col-md-8">
              <?php the_field('contact_left_column'); ?>
            </div>

            <div class="col-md-4">
              <div class="contact-address">
                <?php the_field('contact_right_column'); ?>
              </div>
            </div>
          </div>

          <?php if( have_rows('contact_locations') ): ?>
          <div class="row">
            <?php while( have_rows('contact_locations') ): the_row();
        		// vars
        		$address = get_sub_field('contact_location_address');
            $map = get_sub_field('contact_location_map');

            ?>

            <div class="col-sm-12 col-md-4">
              <div class="contact-address">
                <?php echo $address; ?>
              </div>
            </div>

            <div class="col-sm-12 col-md-8 mb-4">
              <?php echo $map; ?>
            </div>

            <?php endwhile; ?>

          </div>
          <?php endif; ?>
        </div>
      </section>
    </div>

  </main><!-- #main -->
</div><!-- #primary -->

<?php get_footer(); ?>
