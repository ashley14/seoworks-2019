<?php
/**
 ***** Template Name: Service
 *
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @version 1.0
 */

get_header();
$featured_img = get_field('service_custom_background_image');
$featured_default_img = get_template_directory_uri(). '/assets/images/SeoWorks-Generic-Header.jpg' ;?>

<div class="header-content divider-bottom-left" style="background-image:url('<?php if (empty($featured_img)) {echo $featured_default_img; } else { echo $featured_img;} ?>')">
  <div class="container">
    <div class="col-12">
      <?php if (get_field('service_custom_title')) { ?>
      <h2> <?php the_field('service_custom_title');?></h2>
      <?php } ?>
      <?php if (get_field('service_custom_tdescription')) { ?>
      <p><?php the_field('service_custom_tdescription'); ?></p>
      <?php } ?>
    </div>
  </div>
</div>


<div id="primary" class="content-area">
  <main id="main" class="site-main">

  <?php
    while ( have_posts() ) :
      the_post();
  ?>

    <section id="page-content">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="title">
              <h1><?php the_title(); ?></h1>
            </div>

            <div class="content">
              <?php the_content(); ?>
            </div>
          </div>
        </div>
      </div>
    </section>

  <?php
    endwhile; // End of the loop.
  ?>

  </main><!-- #main -->
</div><!-- #primary -->


<?php get_footer(); ?>
