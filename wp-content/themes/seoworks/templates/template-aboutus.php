<?php
/**
 ***** Template Name: About Us
 *
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @version 1.0
 */

 get_header();
 $featured_img = get_field('service_custom_background_image');
 $featured_default_img = get_template_directory_uri(). '/assets/images/SeoWorks-Generic-Header.jpg' ;
 ?>

 <div class="header-content divider-bottom-left" style="background-image:url('<?php if (empty($featured_img)) {echo $featured_default_img; } else { echo $featured_img;} ?>')">
	 <div class="container">
	    <div class="col-12">
	      <?php if (get_field('service_custom_title')) { ?>
	      <h2> <?php the_field('service_custom_title');?></h2>
	      <?php } ?>
	      <?php if (get_field('service_custom_tdescription')) { ?>
	      <p><?php the_field('service_custom_tdescription'); ?></p>
	      <?php } ?>
	    </div>
	  </div>
	</div>

<section class="section">
  <div class="sections">
    <div class="container">
    	<div class="row">
    		<div class="col-12">
          <div class="title">
            <h1><?php the_title(); ?></h1>
          </div>
        </div>
        <div class="col-md-10 offset-md-1">
          <div class="content">
      			<?php the_field('aboutus_block_1'); ?>
          </div>
    		</div>
    	</div>
    </div>
  </div>
</section>

<section class="section section-background__img" style="background-image: url('<?php the_field('aboutus_block_2'); ?>')">
  <div class="sections">
  </div>
</section>

<section class="section">
  <div class="sections">
    <div class="container">
			<?php
			if( have_rows('aboutus_block_3') ):
				while ( have_rows('aboutus_block_3') ) : the_row();
			?>
    	<div class="row">
    		<div class="col-12">
          <div class="title">
            <h2><?php the_sub_field('aboutus_block_3_title'); ?></h2>
          </div>
    		</div>
        <div class="col-md-10 offset-md-1">
          <div class="content">
            <?php the_sub_field('aboutus_block_3_content'); ?>
          </div>
        </div>
    	</div>
			<?php
			endwhile;
				else :
			endif;
			?>
    </div>
  </div>
</section>

<?php
if( have_rows('aboutus_block_4') ):
	while ( have_rows('aboutus_block_4') ) : the_row();
?>
<section class="section google-partners section-background divider-top-left divider-bottom-right" style="background-image: url('<?php the_sub_field('aboutus_block_4_image'); ?>')">
  <!-- <div class="sections"> -->
    <div class="container">
    	<div class="row mt-5 mb-5">
        <div class="col-md-10 offset-md-1">
          <div class="content">
            <?php the_sub_field('aboutus_block_4_content'); ?>
          </div>
        </div>
  		</div>
  	</div>
  <!-- </div> -->
</section>
<?php
endwhile;
	else :
endif;
?>

<section class="section our-values">
  <div class="sections">
    <div class="container-fluid">
			<?php
			if( have_rows('aboutus_block_5') ):
				while ( have_rows('aboutus_block_5') ) : the_row();
			?>
    	<div class="row">

        <div class="col-12">
          <div class="title">
            <h2><?php the_sub_field('aboutus_block_5_title'); ?></h2>
          </div>
        </div>

        <div class="col-md-10 offset-md-1">
          <div class="content">
            <?php the_sub_field('aboutus_block_5_content'); ?>
          </div>
    		</div>

        <div class="values">
          <div class="row offset-xl-2">

						<?php if( have_rows('aboutus_block_5_values') ):
							while( have_rows('aboutus_block_5_values') ): the_row(); ?>
            <div class="col-sm-6 col-md-4 col-xl-2">
              <div class="value">
                <div class="value-icon">
                  <img src="<?php the_sub_field('aboutus_block_5_values__icon'); ?>">
                </div>
                <h4><?php the_sub_field('aboutus_block_5_values__title'); ?></h4>
                <p><?php the_sub_field('aboutus_block_5_values__description'); ?></p>
              </div>
            </div>
						<?php endwhile; ?>
						<?php endif; ?>
          </div>
        </div>

    	</div>
			<?php endwhile; ?>
		<?php endif; ?>

    </div>
  </div>
</section>

<section class="section section-background__color">
  <!-- <div class="sections"> -->
    <div class="container">
			<?php
			if( have_rows('aboutus_block_6') ):
				while ( have_rows('aboutus_block_6') ) : the_row();
			?>
    	<div class="row">
    		<div class="col-12">
          <div class="title">
            <h2><?php the_sub_field('aboutus_block_6_title'); ?></h2>
          </div>
    		</div>
        <div class="col-md-10 offset-md-1">
          <div class="content">
            <?php the_sub_field('aboutus_block_6_content'); ?>
          </div>
        </div>
    	</div>
				<?php endwhile; ?>
			<?php endif; ?>
    </div>
  <!-- </div> -->
</section>

<section class="section">
  <div class="sections">
    <div class="container">
			<?php
			if( have_rows('aboutus_block_7') ):
				while ( have_rows('aboutus_block_7') ) : the_row();
			?>
    	<div class="row">
    		<div class="col-12">
          <div class="title">
            <h2><?php the_sub_field('aboutus_block_7_title'); ?></h2>
          </div>
    		</div>
        <div class="col-md-10 offset-md-1">
          <div class="content">
            <?php the_sub_field('aboutus_block_7_content'); ?>
          </div>
        </div>
    	</div>
				<?php endwhile; ?>
			<?php endif; ?>
    </div>
  </div>
</section>

<section class="section more-services">
  <div class="sections">
    <div class="container">
    	<div class="row">
    		<div class="col-12">
          <div class="title">
            <h3>Find out More</h3>
          </div>
    		</div>
    	</div>
    </div>

    <div class="container">
      <div class="row">
				<?php
				if( have_rows('aboutus_block_8') ):
					while ( have_rows('aboutus_block_8') ) : the_row();
				?>
        <div class="col-md-6">
          <div class="service">
            <div class="service_icon">
              <img src="<?php the_sub_field('aboutus_block_8__icon'); ?>">
            </div>
            <div class="service_content">
              <h4>
								<?php if (get_sub_field('aboutus_block_8__link')) : ?>
								<a href="<?php the_sub_field('aboutus_block_8__link');?>"><?php the_sub_field('aboutus_block_8__title'); ?></a>
								<?php else: ?>
									<?php the_sub_field('aboutus_block_8__title'); ?>
								<?php endif; ?>
							</h4>
              <p><?php the_sub_field('aboutus_block_8__content'); ?></p>
            </div>
          </div>
        </div>

			<?php endwhile; ?>
		<?php endif; ?>
      </div>

    </div>
  </div>
</section>



<?php get_footer(); ?>
