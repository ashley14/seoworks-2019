<?php
/**
 ***** Template Name: Portfolio
 *
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @version 1.0
 */

get_header(); ?>


<?php
$term_name = get_queried_object()->name;
$post_type = 'case-studies';
$post_id = get_the_ID();

// Query Arguments
  $args = array(
  'post_type' => $post_type,
  // 'post_status' => array('publish'),
  'meta_key' => '_wp_page_template',
  'meta_value' => 'single-case-studies-parallax.php',
  'posts_per_page' => 9,
  'order' => 'DESC',
  'orderby' => 'date',
  );

  // The Query
  $caseStudy_post = new WP_Query( $args );
?>

<?php

if( have_rows('case_study_overviews', 5379) ):
// loop through the rows of data
  while ( have_rows('case_study_overviews') ) : the_row();
    echo the_sub_field('case_study_overview');
  endwhile;
  else :
    // no rows found
endif;
?>

<?php
  // The Loop
  if ( $caseStudy_post->have_posts() ) {
  while ( $caseStudy_post->have_posts() ) {
    $caseStudy_post->the_post();

    // if( have_rows('parallax_options') ):
    //   while( have_rows('parallax_options') ): the_row();
    //     // vars
    //     $parallaxOption = get_sub_field('parallax_enable');
    //     $title = get_sub_field('parallax_title');
    //     $description = get_sub_field('parallax_description');
    //     $results = get_sub_field('parallax_results');
    //     $whatWeDid = get_sub_field('parallax_what_we_did');
    //     $clientComments = get_sub_field('parallax_client_comments');
    //     $clientComment = $clientComments['parallax_comment'];
    //     $clientAuthor = $clientComments['parallax_author_comment'];
    //     $imageOptions = get_sub_field('parallax_images');
    //     $backgroundMobile = $imageOptions['parallax_image_mobile_tablet'];
    //     $backgroundDesktop = $imageOptions['parallax_image_desktop'];
    //   endwhile;
    // endif;
    $imageOptions = get_field('parallax_images');
    $backgroundMobile = $imageOptions['parallax_image_mobile_tablet'];
    $backgroundDesktop = $imageOptions['parallax_image_desktop'];

    $caseStudyChallenge = get_field('case_study_challenge');
    $caseStudySolution = get_field('case_study_solution');
    $caseStudyResult = get_field('case_study_result');

?>

<section class="slide">
  <article class="slide_content">
    <figure class="background-mobile">
      <img src="<?php echo $backgroundMobile; ?>" alt="">
    </figure>
    <div class="slide_content-text">
      <div class="slide_content-info">
        <div class="title">
          <h1><?php echo the_title(); ?></h1>
        </div>
      </div>
      <div class="slide_content-challenge">
        <h2>Challenge</h2>
        <?php echo $caseStudyChallenge; ?>
      </div>
      <div class="slide_content-solution">
        <h2>Solution</h2>
        <?php echo $caseStudySolution; ?>
      </div>
      <div class="slide_content-result">
        <h2>Result</h2>
        <?php echo $caseStudyResult;  ?>
      </div>
      <a class="btn btn-green" href="<?php echo get_home_url(); ?>/contact-us/">Request a quote</a>
    </div>
    <div class="background" style="background: url(<?php echo $backgroundDesktop; ?>)"></div>
  </article>
</section>


<?php

    }
  } else {
  // no posts found
  }
  /* Restore original Post Data */
  wp_reset_postdata();
?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/TweenMax.min.js"></script>
		 <script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/ScrollMagic.min.js"></script>
		 <script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/plugins/animation.gsap.min.js"></script>
		 <script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/plugins/debug.addIndicators.min.js"></script>

     <script type="text/javascript">
     $(function () { // wait for document ready
		// init
		var controller = new ScrollMagic.Controller({
			globalSceneOptions: {
				triggerHook: 'onLeave',
				duration: "0" // this works just fine with duration 0 as well
				// However with large numbers (>20) of pinned sections display errors can occur so every section should be unpinned once it's covered by the next section.
				// Normally 100% would work for this, but here 200% is used, as  3 is shown for more than 100% of scrollheight due to the pause.
			}
		});

		// get all slides
		var slides = document.querySelectorAll("section.slide");

		// create scene for every slide
		for (var i=0; i<slides.length; i++) {
			new ScrollMagic.Scene({
					triggerElement: slides[i]
				})
				.setPin(slides[i], {pushFollowers: false})
				// .addIndicators() // add indicators (requires plugin)
				.addTo(controller);
        if ($( window ).width() <= 768) {
          controller.enabled(false);
        }
		}
	});

     </script>

<?php get_footer(); ?>
