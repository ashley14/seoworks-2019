<?php
/**
 ***** Template Name: Blog
 *
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @version 1.0
 */

get_header();
get_header();
$featured_img = get_the_post_thumbnail_url(get_the_ID(),'full');
$featured_default_img = get_template_directory_uri(). '/assets/images/SeoWorks-Generic-Header.jpg' ;
?>

<div class="blog">
  <div class="header-content divider-bottom-left" style="background-image:url('<?php if (empty($featured_img)) {echo $featured_default_img; } else { echo $featured_img;} ?>')">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <?php
            while ( have_posts() ) :
              the_post();
          ?>
          <div class="title">
              <h1><?php the_title(); ?></h1>
          </div>

          <p><?php the_content(); ?></p>
          <?php
            endwhile; // End of the loop.
          ?>
        </div>
      </div>
    </div>
  </div>

  <div id="primary" class="content-area">
    <main id="main" class="site-main">
      <section>
        <div class="sections">
          <div class="container">
            <div class="row">
              <?php
              $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
              // Query Arguments
                $args = array(
                'post_type' => array('post'),
                'post_status' => array('publish'),
                'posts_per_page' => 9,
                'order' => 'DESC',
                'orderby' => 'date',
                'paged' => $paged,
                );


                // The Query
                $blog_post = new WP_Query( $args );

                // The Loop
                if ( $blog_post->have_posts() ) {
                while ( $blog_post->have_posts() ) {
                  $blog_post->the_post(); ?>

                  <article class="col-sm-6 col-xl-4">
                    <div class="card">
                      <div class="card-image">
                        <figure>
                          <a href="<?php the_permalink(); ?>">
                            <?php the_post_thumbnail('thumb-blog'); ?>
                          </a>
                        </figure>
                      </div>
                      <div class="card-body">

                        <h5 class="card-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                        <div class="card-text"><?php the_excerpt(); ?></div>
                        <p class="card-date">Date posted: <span><?php the_date(); ?></span></p>
                        <a href="<?php the_permalink(); ?>" class="btn btn-readmore_blog">Read More</a>
                      </div>
                    </div>
                  </article>

              <?php
                  }
                } else {
                // no posts found
                }
                /* Restore original Post Data */
                wp_reset_postdata();
              ?>

              <?php if (function_exists("pagination")) {
                  pagination($blog_post->max_num_pages);
              } ?>

            </div>
          </div>
        </div>
      </section>
    </main><!-- #main -->
  </div><!-- #primary -->
</div>

<?php get_footer(); ?>
