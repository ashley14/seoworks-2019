<?php
/**
 ***** Template Name: Services Page
 *
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @version 1.0
 */

get_header();
$featured_img = get_field('service_custom_background_image');
$featured_default_img = get_template_directory_uri(). '/assets/images/SeoWorks-Generic-Header.jpg';
?>

<div class="header-content divider-bottom-left" style="background-image:url('<?php if (empty($featured_img)) {echo $featured_default_img; } else { echo $featured_img;} ?>')">
  <div class="container">
    <div class="col-12">
      <?php if (get_field('service_custom_title')) { ?>
      <h2> <?php the_field('service_custom_title');?></h2>
      <?php } ?>
      <?php if (get_field('service_custom_tdescription')) { ?>
      <p><?php the_field('service_custom_tdescription'); ?></p>
      <?php } ?>
    </div>
  </div>
</div>


<div id="primary" class="content-area">
  <main id="main" class="site-main">

  <?php
    while ( have_posts() ) :
      the_post();
  ?>

    <section id="page-content" class="services">
      <div class="sections-tb">

        <div class="container">
          <div class="row">
            <div class="col-12">
              <div class="services-grid">
                <div class="container">
                  <div class="row no-gutters">
                    <?php
                    // Query Arguments
                      $args = array (
                        'post_type' => 'page',
                        'meta_query' => array(
                          // selecting pages which has 'service' as page template
                          array (
                            'key'   => '_wp_page_template',
                            'value' => array(
                              'templates/template-service.php',
                              'templates/template-service-v2.php'
                            )
                          ),

                          // Showing only services page that have the option in yes to show on the homepage
                          // array(
                          //   'key'	 	=> 'show_service_homepage',
                          //   'value'	  	=> 'yes')
                        ),
                        'post_status' => array('publish'),
                        'posts_per_page' => -1,
                        'order' => 'DESC',
                        'orderby' => 'date',
                      );

                      // The Query
                      $services = new WP_Query( $args );

                      // The Loop
                      if ( $services->have_posts() ) {
                      while ( $services->have_posts() ) {
                        $services->the_post();
                        $featured_service_img_url = get_the_post_thumbnail_url(get_the_ID(),'thumb-services');?>

                        <div class="col-sm-6 col-md-6 col-lg-4">
                          <div class="item service" style="background-image:url('<?php echo esc_url($featured_service_img_url); ?>')">
                            <!-- SERVICE TITLE -->
                            <h3><?php the_title() ?></h3>
                            <!-- HOVER EFFECT -->
                            <a href="<?php the_permalink(); ?>" class="service-hover__link">
                              <div class="service-hover">
                                <!-- HOVER SERVICES TITLE -->
                                <h5><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></h5>
                                <!-- HOVER SERVICES CONTENT -->
                                <?php if( get_field('service_custom_excerpt') ): ?>
                                  <?php the_field('service_custom_excerpt'); ?></p>
                                <?php else: ?>
                                <?php the_excerpt(); ?>
                                <?php endif; ?>
                              </div>
                            </a>
                          </div>
                        </div>

                      <?php
                      }
                      } else {
                      // no posts found
                      }
                      /* Restore original Post Data */
                      wp_reset_postdata();
                    ?>

                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </section>

  <?php
    endwhile; // End of the loop.
  ?>

  </main><!-- #main -->
</div><!-- #primary -->


<?php get_footer(); ?>
