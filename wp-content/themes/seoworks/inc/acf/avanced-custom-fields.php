<?php

// ACF  Case Studies Information
require get_template_directory() . '/inc/acf/acf-case-study-information.php';

// ACF  Custom Header
require get_template_directory() . '/inc/acf/acf-custom-header.php';

// ACF  Custom option to show service in the homepage
require get_template_directory() . '/inc/acf/acf-service-homepage-option.php';

// ACF  Custom option to add custom excerpt service page to show the homepage
require get_template_directory() . '/inc/acf/acf-service-homepage-excerpt.php';

// ACF  Custom option to show casae study in the homepage
require get_template_directory() . '/inc/acf/acf-casestudy-homepage-option.php';

// ACF  Custom Slider HomePage
// require get_template_directory() . '/inc/acf/acf-homepage-slider.php';

// ACF  Custom option for the contact Page
require get_template_directory() . '/inc/acf/acf-contact-us.php';

// ACF Custom options for the About Us Page
require get_template_directory() . '/inc/acf/acf-aboutus-optionpage.php';

// ACF general theme options
require get_template_directory() . '/inc/acf/acf-option-page.php';

// ACF General theme options for Case Studies Archive Page
require get_template_directory() . '/inc/acf/acf-option-page_case-studies-archive.php';

// ACF Custom Options for HomePage
require get_template_directory() . '/inc/acf/acf-homepage-optionpage.php';

// ACF Custom Options for the footer
require get_template_directory() . '/inc/acf/acf-footer-options.php';

// ACF Custom Options for the Reviews archive page
require get_template_directory() . '/inc/acf/acf-option-page-review-archive.php';

// ACF Custom Options for the carousel in the Case Studies taxonomy
require get_template_directory() . '/inc/acf/acf-case-study-carousel-taxonomy.php';

// ACF Cutom Options for the case studies which are showed with a parallax stylesheets
require get_template_directory() . '/inc/acf/acf-case-study-parallax-options.php';







?>
