<?php

// CPT Slider Clients - HomePage
require get_template_directory() . '/inc/cpt/cpt-clients.php';

// CPT Slider Awards - HomePage
require get_template_directory() . '/inc/cpt/cpt-awards.php';

// CPT Reviews
require get_template_directory() . '/inc/cpt/cpt-reviews.php';

// CPT Portfolio
require get_template_directory() . '/inc/cpt/cpt-portfolio.php';

// CPT Services
// require get_template_directory() . '/inc/cpt/cpt-services.php';

?>
