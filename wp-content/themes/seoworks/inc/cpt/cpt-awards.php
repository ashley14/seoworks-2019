<?php
// Register Custom Post Type Award
// Post Type Key: award
function create_award_cpt() {

	$labels = array(
		'name' => __( 'Awards', 'Post Type General Name', 'seoworks.co.uk' ),
		'singular_name' => __( 'Award', 'Post Type Singular Name', 'seoworks.co.uk' ),
		'menu_name' => __( 'Awards', 'seoworks.co.uk' ),
		'name_admin_bar' => __( 'Award', 'seoworks.co.uk' ),
		'archives' => __( 'Award Archives', 'seoworks.co.uk' ),
		'attributes' => __( 'Award Attributes', 'seoworks.co.uk' ),
		'parent_item_colon' => __( 'Parent Award:', 'seoworks.co.uk' ),
		'all_items' => __( 'All Awards', 'seoworks.co.uk' ),
		'add_new_item' => __( 'Add New Award', 'seoworks.co.uk' ),
		'add_new' => __( 'Add New', 'seoworks.co.uk' ),
		'new_item' => __( 'New Award', 'seoworks.co.uk' ),
		'edit_item' => __( 'Edit Award', 'seoworks.co.uk' ),
		'update_item' => __( 'Update Award', 'seoworks.co.uk' ),
		'view_item' => __( 'View Award', 'seoworks.co.uk' ),
		'view_items' => __( 'View Awards', 'seoworks.co.uk' ),
		'search_items' => __( 'Search Award', 'seoworks.co.uk' ),
		'not_found' => __( 'Not found', 'seoworks.co.uk' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'seoworks.co.uk' ),
		'featured_image' => __( 'Featured Image', 'seoworks.co.uk' ),
		'set_featured_image' => __( 'Set featured image', 'seoworks.co.uk' ),
		'remove_featured_image' => __( 'Remove featured image', 'seoworks.co.uk' ),
		'use_featured_image' => __( 'Use as featured image', 'seoworks.co.uk' ),
		'insert_into_item' => __( 'Insert into Award', 'seoworks.co.uk' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Award', 'seoworks.co.uk' ),
		'items_list' => __( 'Awards list', 'seoworks.co.uk' ),
		'items_list_navigation' => __( 'Awards list navigation', 'seoworks.co.uk' ),
		'filter_items_list' => __( 'Filter Awards list', 'seoworks.co.uk' ),
	);
	$args = array(
		'label' => __( 'Award', 'seoworks.co.uk' ),
		'description' => __( 'Seo Works Awards', 'seoworks.co.uk' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-awards',
		'supports' => array('title', 'thumbnail', 'author', ),
		'taxonomies' => array(),
		'public' => false,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => false,
		'hierarchical' => false,
		'exclude_from_search' => true,
		'show_in_rest' => true,
		'publicly_queryable' => false,
		'capability_type' => 'post',
	);
	register_post_type( 'award', $args );

}
add_action( 'init', 'create_award_cpt', 0 );
?>
