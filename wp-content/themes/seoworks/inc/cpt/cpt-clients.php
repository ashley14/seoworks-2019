<?php
// Register Custom Post Type Client
// Post Type Key: client
function create_client_cpt() {

	$labels = array(
		'name' => __( 'Clients', 'Post Type General Name', 'seoworks.co.uk' ),
		'singular_name' => __( 'Client', 'Post Type Singular Name', 'seoworks.co.uk' ),
		'menu_name' => __( 'Clients', 'seoworks.co.uk' ),
		'name_admin_bar' => __( 'Client', 'seoworks.co.uk' ),
		'archives' => __( 'Client Archives', 'seoworks.co.uk' ),
		'attributes' => __( 'Client Attributes', 'seoworks.co.uk' ),
		'parent_item_colon' => __( 'Parent Client:', 'seoworks.co.uk' ),
		'all_items' => __( 'All Clients', 'seoworks.co.uk' ),
		'add_new_item' => __( 'Add New Client', 'seoworks.co.uk' ),
		'add_new' => __( 'Add New', 'seoworks.co.uk' ),
		'new_item' => __( 'New Client', 'seoworks.co.uk' ),
		'edit_item' => __( 'Edit Client', 'seoworks.co.uk' ),
		'update_item' => __( 'Update Client', 'seoworks.co.uk' ),
		'view_item' => __( 'View Client', 'seoworks.co.uk' ),
		'view_items' => __( 'View Clients', 'seoworks.co.uk' ),
		'search_items' => __( 'Search Client', 'seoworks.co.uk' ),
		'not_found' => __( 'Not found', 'seoworks.co.uk' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'seoworks.co.uk' ),
		'featured_image' => __( 'Featured Image', 'seoworks.co.uk' ),
		'set_featured_image' => __( 'Set featured image', 'seoworks.co.uk' ),
		'remove_featured_image' => __( 'Remove featured image', 'seoworks.co.uk' ),
		'use_featured_image' => __( 'Use as featured image', 'seoworks.co.uk' ),
		'insert_into_item' => __( 'Insert into Client', 'seoworks.co.uk' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Client', 'seoworks.co.uk' ),
		'items_list' => __( 'Clients list', 'seoworks.co.uk' ),
		'items_list_navigation' => __( 'Clients list navigation', 'seoworks.co.uk' ),
		'filter_items_list' => __( 'Filter Clients list', 'seoworks.co.uk' ),
	);
	$args = array(
		'label' => __( 'Client', 'seoworks.co.uk' ),
		'description' => __( 'Seo Works clients', 'seoworks.co.uk' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-analytics',
		'supports' => array('title', 'thumbnail', 'author', ),
		'taxonomies' => array(),
		'public' => false,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => false,
		'hierarchical' => false,
		'exclude_from_search' => true,
		'show_in_rest' => true,
		'publicly_queryable' => false,
		'capability_type' => 'post',
	);
	register_post_type( 'client', $args );

}
add_action( 'init', 'create_client_cpt', 0 );
?>
