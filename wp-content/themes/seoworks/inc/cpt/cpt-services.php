<?php
// Register Custom Post Type Service
// Post Type Key: service
function create_service_cpt() {

	$labels = array(
		'name' => __( 'Services', 'Post Type General Name', 'seoworks.co.uk' ),
		'singular_name' => __( 'Service', 'Post Type Singular Name', 'seoworks.co.uk' ),
		'menu_name' => __( 'Services', 'seoworks.co.uk' ),
		'name_admin_bar' => __( 'Service', 'seoworks.co.uk' ),
		'archives' => __( 'Service Archives', 'seoworks.co.uk' ),
		'attributes' => __( 'Service Attributes', 'seoworks.co.uk' ),
		'parent_item_colon' => __( 'Parent Service:', 'seoworks.co.uk' ),
		'all_items' => __( 'All Services', 'seoworks.co.uk' ),
		'add_new_item' => __( 'Add New Service', 'seoworks.co.uk' ),
		'add_new' => __( 'Add New', 'seoworks.co.uk' ),
		'new_item' => __( 'New Service', 'seoworks.co.uk' ),
		'edit_item' => __( 'Edit Service', 'seoworks.co.uk' ),
		'update_item' => __( 'Update Service', 'seoworks.co.uk' ),
		'view_item' => __( 'View Service', 'seoworks.co.uk' ),
		'view_items' => __( 'View Services', 'seoworks.co.uk' ),
		'search_items' => __( 'Search Service', 'seoworks.co.uk' ),
		'not_found' => __( 'Not found', 'seoworks.co.uk' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'seoworks.co.uk' ),
		'featured_image' => __( 'Featured Image', 'seoworks.co.uk' ),
		'set_featured_image' => __( 'Set featured image', 'seoworks.co.uk' ),
		'remove_featured_image' => __( 'Remove featured image', 'seoworks.co.uk' ),
		'use_featured_image' => __( 'Use as featured image', 'seoworks.co.uk' ),
		'insert_into_item' => __( 'Insert into Service', 'seoworks.co.uk' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Service', 'seoworks.co.uk' ),
		'items_list' => __( 'Services list', 'seoworks.co.uk' ),
		'items_list_navigation' => __( 'Services list navigation', 'seoworks.co.uk' ),
		'filter_items_list' => __( 'Filter Services list', 'seoworks.co.uk' ),
	);
	$args = array(
		'label' => __( 'Service', 'seoworks.co.uk' ),
		'description' => __( 'Seo Works Services', 'seoworks.co.uk' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-chart-area',
		'supports' => array('title', 'editor', 'excerpt', 'thumbnail', 'revisions', 'author', 'custom-fields', ),
		'taxonomies' => array('sector', ),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => true,
		'hierarchical' => true,
		'exclude_from_search' => true,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
	);
	register_post_type( 'service', $args );

}
add_action( 'init', 'create_service_cpt', 0 );
?>
