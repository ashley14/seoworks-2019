<?php
// Register Custom Post Type Review
// Post Type Key: review
function create_review_cpt() {

	$labels = array(
		'name' => __( 'Reviews', 'Post Type General Name', 'seoworks.co.uk' ),
		'singular_name' => __( 'Review', 'Post Type Singular Name', 'seoworks.co.uk' ),
		'menu_name' => __( 'Reviews', 'seoworks.co.uk' ),
		'name_admin_bar' => __( 'Review', 'seoworks.co.uk' ),
		'archives' => __( 'Review Archives', 'seoworks.co.uk' ),
		'attributes' => __( 'Review Attributes', 'seoworks.co.uk' ),
		'parent_item_colon' => __( 'Parent Review:', 'seoworks.co.uk' ),
		'all_items' => __( 'All Reviews', 'seoworks.co.uk' ),
		'add_new_item' => __( 'Add New Review', 'seoworks.co.uk' ),
		'add_new' => __( 'Add New', 'seoworks.co.uk' ),
		'new_item' => __( 'New Review', 'seoworks.co.uk' ),
		'edit_item' => __( 'Edit Review', 'seoworks.co.uk' ),
		'update_item' => __( 'Update Review', 'seoworks.co.uk' ),
		'view_item' => __( 'View Review', 'seoworks.co.uk' ),
		'view_items' => __( 'View Reviews', 'seoworks.co.uk' ),
		'search_items' => __( 'Search Review', 'seoworks.co.uk' ),
		'not_found' => __( 'Not found', 'seoworks.co.uk' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'seoworks.co.uk' ),
		'featured_image' => __( 'Featured Image', 'seoworks.co.uk' ),
		'set_featured_image' => __( 'Set featured image', 'seoworks.co.uk' ),
		'remove_featured_image' => __( 'Remove featured image', 'seoworks.co.uk' ),
		'use_featured_image' => __( 'Use as featured image', 'seoworks.co.uk' ),
		'insert_into_item' => __( 'Insert into Review', 'seoworks.co.uk' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Review', 'seoworks.co.uk' ),
		'items_list' => __( 'Reviews list', 'seoworks.co.uk' ),
		'items_list_navigation' => __( 'Reviews list navigation', 'seoworks.co.uk' ),
		'filter_items_list' => __( 'Filter Reviews list', 'seoworks.co.uk' ),
	);
	$args = array(
		'label' => __( 'Review', 'seoworks.co.uk' ),
		'description' => __( 'Seo Works Reviews', 'seoworks.co.uk' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-editor-quote',
		'supports' => array('title', 'editor', 'thumbnail', 'revisions', 'author', ),
		'taxonomies' => array(),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'hierarchical' => true,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
		'rewrite'     => true,
    'has_archive' => 'reviews',
	);
	register_post_type( 'review', $args );

}
add_action( 'init', 'create_review_cpt', 0 );
?>
