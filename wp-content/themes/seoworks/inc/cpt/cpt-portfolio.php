<?php
// Register Custom Post Type Case Study
// Post Type Key: casestudy
function create_casestudy_cpt() {

	$labels = array(
		'name' => __( 'Case Studies', 'Post Type General Name', 'seoworks.co.uk' ),
		'singular_name' => __( 'Case Study', 'Post Type Singular Name', 'seoworks.co.uk' ),
		'menu_name' => __( 'Case Studies', 'seoworks.co.uk' ),
		'name_admin_bar' => __( 'Case Study', 'seoworks.co.uk' ),
		'archives' => __( 'Case Study Archives', 'seoworks.co.uk' ),
		'attributes' => __( 'Case Study Attributes', 'seoworks.co.uk' ),
		'parent_item_colon' => __( 'Parent Case Study:', 'seoworks.co.uk' ),
		'all_items' => __( 'All Case Studies', 'seoworks.co.uk' ),
		'add_new_item' => __( 'Add New Case Study', 'seoworks.co.uk' ),
		'add_new' => __( 'Add New', 'seoworks.co.uk' ),
		'new_item' => __( 'New Case Study', 'seoworks.co.uk' ),
		'edit_item' => __( 'Edit Case Study', 'seoworks.co.uk' ),
		'update_item' => __( 'Update Case Study', 'seoworks.co.uk' ),
		'view_item' => __( 'View Case Study', 'seoworks.co.uk' ),
		'view_items' => __( 'View Case Studies', 'seoworks.co.uk' ),
		'search_items' => __( 'Search Case Study', 'seoworks.co.uk' ),
		'not_found' => __( 'Not found', 'seoworks.co.uk' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'seoworks.co.uk' ),
		'featured_image' => __( 'Featured Image', 'seoworks.co.uk' ),
		'set_featured_image' => __( 'Set featured image', 'seoworks.co.uk' ),
		'remove_featured_image' => __( 'Remove featured image', 'seoworks.co.uk' ),
		'use_featured_image' => __( 'Use as featured image', 'seoworks.co.uk' ),
		'insert_into_item' => __( 'Insert into Case Study', 'seoworks.co.uk' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Case Study', 'seoworks.co.uk' ),
		'items_list' => __( 'Case Studies list', 'seoworks.co.uk' ),
		'items_list_navigation' => __( 'Case Studies list navigation', 'seoworks.co.uk' ),
		'filter_items_list' => __( 'Filter Case Studies list', 'seoworks.co.uk' ),
		'featured_image' => __( 'Company Logo', 'seoworks.co.uk' ),
		'set_featured_image' => __( 'Set company logo', 'seoworks.co.uk' ),
		'remove_featured_image' => __( 'Remove company logo', 'seoworks.co.uk' ),
		'use_featured_image' => __( 'Use as company logo', 'seoworks.co.uk' ),
	);
	$args = array(
		'label' => __( 'Case Study', 'seoworks.co.uk' ),
		'description' => __( 'Seo Works Case Study', 'seoworks.co.uk' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-pressthis',
		'supports' => array('title', 'editor', 'thumbnail', 'revisions', 'author', ),
		'taxonomies' => array(),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => true,
		'hierarchical' => true,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
		'rewrite'     => array( 'slug' => 'case-studies' ),
    'has_archive' => 'search-engine-optimisation-testimonials',
	);
	register_post_type( 'case-studies', $args );

}
add_action( 'init', 'create_casestudy_cpt', 0 );

// Register Taxonomy Sector
// Taxonomy Key: sector
function create_sector_tax() {

	$labels = array(
		'name'              => _x( 'Sectors', 'taxonomy general name', 'seoworks.co.uk' ),
		'singular_name'     => _x( 'Sector', 'taxonomy singular name', 'seoworks.co.uk' ),
		'search_items'      => __( 'Search Sectors', 'seoworks.co.uk' ),
		'all_items'         => __( 'All Sectors', 'seoworks.co.uk' ),
		'parent_item'       => __( 'Parent Sector', 'seoworks.co.uk' ),
		'parent_item_colon' => __( 'Parent Sector:', 'seoworks.co.uk' ),
		'edit_item'         => __( 'Edit Sector', 'seoworks.co.uk' ),
		'update_item'       => __( 'Update Sector', 'seoworks.co.uk' ),
		'add_new_item'      => __( 'Add New Sector', 'seoworks.co.uk' ),
		'new_item_name'     => __( 'New Sector Name', 'seoworks.co.uk' ),
		'menu_name'         => __( 'Sector', 'seoworks.co.uk' ),
	);
	$args = array(
		'labels' => $labels,
		'description' => __( 'Sectors of the Case Studies', 'seoworks.co.uk' ),
		'hierarchical' => true,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'show_in_rest' => false,
		'show_tagcloud' => false,
		'show_in_quick_edit' => true,
		'show_admin_column' => true,
	);
	register_taxonomy( 'sector', array('case-studies', ), $args );

}
add_action( 'init', 'create_sector_tax' );

// Register Taxonomy Campaign
// Taxonomy Key: campaign
function create_campaign_tax() {

	$labels = array(
		'name'              => _x( 'Campaigns', 'taxonomy general name', 'seoworks.co.uk' ),
		'singular_name'     => _x( 'Campaign', 'taxonomy singular name', 'seoworks.co.uk' ),
		'search_items'      => __( 'Search Campaigns', 'seoworks.co.uk' ),
		'all_items'         => __( 'All Campaigns', 'seoworks.co.uk' ),
		'parent_item'       => __( 'Parent Campaign', 'seoworks.co.uk' ),
		'parent_item_colon' => __( 'Parent Campaign:', 'seoworks.co.uk' ),
		'edit_item'         => __( 'Edit Campaign', 'seoworks.co.uk' ),
		'update_item'       => __( 'Update Campaign', 'seoworks.co.uk' ),
		'add_new_item'      => __( 'Add New Campaign', 'seoworks.co.uk' ),
		'new_item_name'     => __( 'New Campaign Name', 'seoworks.co.uk' ),
		'menu_name'         => __( 'Campaign', 'seoworks.co.uk' ),
	);
	$args = array(
		'labels' => $labels,
		'description' => __( 'Campaigns of the Case Studies', 'seoworks.co.uk' ),
		'hierarchical' => true,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'show_in_rest' => false,
		'show_tagcloud' => true,
		'show_in_quick_edit' => true,
		'show_admin_column' => true,
	);
	register_taxonomy( 'campaign', array('case-studies', ), $args );

}
add_action( 'init', 'create_campaign_tax' );

?>
