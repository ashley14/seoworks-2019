<?php

function shortcode_casestudies($atts, $content = null) {
  extract( shortcode_atts( array(
      'category' => 'category',
      'slug' => 'slug',
      ), $atts ) );
  ob_start();?>

  <!-- <div class="container"> -->
  <div class="sections shortcode-casestudies">

    <div class="row">
      <?php
      $post_type = 'case-studies';

      // Query Arguments
        $args = array(
        'post_type' => $post_type,
        'post_status' => array('publish'),
        'tax_query' => array(
          // 'relation' => 'OR',
          array(
              'taxonomy' => esc_attr($category),
              'field'    => 'slug',
              'terms'    => esc_attr($slug)
            ),
          ),
        'posts_per_page' => 3,
        'order' => 'DESC',
        'orderby' => 'date',
        );

        // The Query
        $casestudy_post = new WP_Query( $args );

        // The Loop
        if ( $casestudy_post->have_posts() ) {
        while ( $casestudy_post->have_posts() ) {
          $casestudy_post->the_post(); ?>

          <article class="col-sm-6 col-xl-4">
            <div class="card">
              <div class="card-image">
                <figure>
                  <a href="<?php the_permalink(); ?>">
                    <?php if (class_exists('MultiPostThumbnails')) :
                      MultiPostThumbnails::the_post_thumbnail(
                          get_post_type(),
                          'second-image-casestudy',
                          add_image_size('thumb-blog')
                      );
                    endif; ?>
                  </a>
                </figure>
              </div>
              <div class="card-body">
                <!-- <p class="card-date">Date posted: <span><?php //the_date(); ?></span></p> -->
                <h5 class="card-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                <div class="card-text"><?php the_excerpt(); ?></div>
                <a href="<?php the_permalink(); ?>" class="btn btn-readmore_blog">Read More</a>
              </div>
            </div>
          </article>

      <?php  }
        } else {
        // no posts found
        }
        /* Restore original Post Data */
        wp_reset_postdata();
      ?>

    </div>

  </div>

  <!-- </div> -->

<?php
  return ob_get_clean();
}
    add_shortcode( 'case-studies', 'shortcode_casestudies' );
?>
