<?php

function shortcode_slider_awards($atts, $content = null) {
  ob_start();?>

  <section class="shortcode-awards">
    <!-- <div class="sections"> -->
      <?php
      // Query Arguments
        $args = array(
        'post_type' => array('award'),
        'posts_per_page' => -1,
        'order' => 'DESC',
        'orderby' => 'date',
        );
        // The Query
        $slider_clients = new WP_Query( $args ); ?>
      <!-- <div class="container"> -->
        <div class="row">
          <div class="col-12">
            <div class="owl-carousel carousel-awards">
              <?php // The Loop
                if ( $slider_clients->have_posts() ) {
                  while ( $slider_clients->have_posts() ) {
                    $slider_clients->the_post();
                    if ( has_post_thumbnail() ) { ?>
                      <div class="carousel-client">
                        <figure>
                          <?php the_post_thumbnail( 'thumb-sliders' ); ?>
                        </figure>
                      </div>
                      <?php
                    }
                  }
                } else {
                  // no posts found
                  }
                /* Restore original Post Data */
                wp_reset_postdata();
            ?>
            </div>
          </div>
        </div>
      <!-- </div> -->
    <!-- </div> -->
  </section>

<?php
  return ob_get_clean();
}
    add_shortcode( 'awards-slider', 'shortcode_slider_awards' );
?>
