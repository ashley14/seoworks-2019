<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package SeoWorks
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">

		<?php
			get_template_part( 'template-parts/footer/footer', 'call-to-action');

			get_template_part( 'template-parts/footer/footer', 'awards');

			get_template_part( 'template-parts/footer/footer', 'widgets');

			get_template_part( 'template-parts/footer/footer', 'site-info');

		 ?>

	</footer><!-- #colophon -->
	<div id="back-top-top"></div>
</div><!-- #page -->

<?php wp_footer(); ?>

	<!-- Tracking Codes -->

		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-9188111-41"></script>
		<script>
		 window.dataLayer = window.dataLayer || [];
		 function gtag(){dataLayer.push(arguments);}
		 gtag('js', new Date());
		 gtag('config', 'UA-9188111-41');
		</script>
		<!-- END Global site tag (gtag.js) - Google Analytics -->

		<!-- Global site tag (gtag.js) - Google Ads: 931141469 -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=AW-931141469"></script>
		<script>
		 window.dataLayer = window.dataLayer || [];
		 function gtag(){dataLayer.push(arguments);}
		 gtag('js', new Date());
		 gtag('config', 'AW-931141469');
		</script>
		<!-- END Global site tag (gtag.js) - Google Ads: 931141469 -->

		<!-- Google Tag Manager -->
			<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-54R22D"
			height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
			<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
			new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
			j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
			'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
			})(window,document,'script','dataLayer','GTM-54R22D');</script>
		<!-- End Google Tag Manager -->

		<!-- Google Call analytics -->
		<script>
			gtag('config', 'AW-931141469/CcgUCMWPwYgBEN2ugLwD', {
			 'phone_conversion_number': '0800 292 2410'
			});
		</script>
		<!-- End call analytics -->

		<!--- Bing Tracking Code -->
			<script>(function(w,d,t,r,u){var f,n,i;w[u]=w[u]||[],f=function(){var o={ti:"11002056"};o.q=w[u],w[u]=new UET(o),w[u].push("pageLoad")},n=d.createElement(t),n.src=r,n.async=1,n.onload=n.onreadystatechange=function(){var s=this.readyState;s&&s!=="loaded"&&s!=="complete"||(f(),n.onload=n.onreadystatechange=null)},i=d.getElementsByTagName(t)[0],i.parentNode.insertBefore(n,i)})(window,document,"script","//bat.bing.com/bat.js","uetq");</script>
		<!--- END Bing Tracking Code -->

		<!--Start of Tawk.to Script-->
			<script type="text/javascript">
			var $_Tawk_API={},$_Tawk_LoadStart=new Date();
			(function(){
			var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
			s1.async=true;
			s1.src='https://embed.tawk.to/57cee75f3bec6867d93c9d28/default';
			s1.charset='UTF-8';
			s1.setAttribute('crossorigin','*');
			s0.parentNode.insertBefore(s1,s0);
			})();
			</script>
		<!--End of Tawk.to Script-->

		<!-- Google Code for SEO Works Contact Form Conversion Page -->
		<?php if ( is_page('6458') ) { ?>
			<script>
			 gtag('event', 'conversion', {'send_to': 'AW-931141469/7ghSCNOvwYgBEN2ugLwD'});
			</script>
		<?php } ?>
		<!-- END Google Code for SEO Works Contact Form Conversion Page -->

		<!-- Event snippet for PPC Audit - New conversion page -->
		<?php if ( is_page('6977') ) { ?>
			<script>
			 gtag('event', 'conversion', {'send_to': 'AW-931141469/dcAtCMe-2IgBEN2ugLwD'});
			</script>
		<?php } ?>
		<!-- END vent snippet for PPC Audit - New conversion page -->


	<!-- END Tracking Codes -->
	</head>
	<!-- LinkedIn Insight Tag -->
		<script type="text/javascript">
		_linkedin_partner_id = "301890";
		window._linkedin_data_partner_ids = window._linkedin_data_partner_ids || [];
		window._linkedin_data_partner_ids.push(_linkedin_partner_id);
		</script><script type="text/javascript">
		(function(){var s = document.getElementsByTagName("script")[0];
		var b = document.createElement("script");
		b.type = "text/javascript";b.async = true;
		b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
		s.parentNode.insertBefore(b, s);})();
		</script>
		<noscript>
		<img height="1" width="1" style="display:none;" alt="" src="https://dc.ads.linkedin.com/collect/?pid=301890&fmt=gif" />
		</noscript>
	<!-- END LinkedIn Insight Tag -->

	<!-- Facebook Pixel Code -->
	<script>
	!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
	n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
	document,'script','https://connect.facebook.net/en_US/fbevents.js');
	fbq('init', '1848938952002812'); // Insert your pixel ID here.
	fbq('track', 'PageView');
	</script>
	<noscript><img data-lazyloaded="1" src="data:image/gif;base64,R0lGODdhAQABAPAAAMPDwwAAACwAAAAAAQABAAACAkQBADs=" height="1" width="1" style="display:none"
	src="https://www.facebook.com/tr?id=1848938952002812&ev=PageView&noscript=1"
	/><noscript><img height="1" width="1" style="display:none"
	src="https://www.facebook.com/tr?id=1848938952002812&ev=PageView&noscript=1"
	/></noscript></noscript>
	<!-- DO NOT MODIFY -->
	<!-- End Facebook Pixel Code -->

	<!-- Google Remarketing Tag -->
	<script type="text/javascript">
	/* <![CDATA[ */
	var google_conversion_id = 931141469;
	var google_custom_params = window.google_tag_params;
	var google_remarketing_only = true;
	/* ]]> */
	</script>
	<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
	</script>
	<noscript>
	<div style="display:inline;">
	<img data-lazyloaded="1" src="data:image/gif;base64,R0lGODdhAQABAPAAAMPDwwAAACwAAAAAAQABAAACAkQBADs=" height="1" width="1" style="border-style:none;" alt="" data-src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/931141469/?guid=ON&amp;script=0"/><noscript><img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/931141469/?guid=ON&amp;script=0"/></noscript>
	</div>
	</noscript>
	<!-- End Google Remarketing Tag -->

	<!-- Lead tracking Tag -->
	<script src="//adpxl.co/i3uOaUAG/an.js"></script><noscript><img src="//adpxl.co/i3uOaUAG/spacer.gif" alt=""></noscript>
	<!-- End Lead tracking Tag -->

	<!-- Adroll-->
	<script type="text/javascript">
    adroll_adv_id = "5EKBNTDTWJE2FDJWRDMCN6";
    adroll_pix_id = "FJNSXZBFTVAQZPVCRGOFTX";

    (function () {
        var _onload = function(){
            if (document.readyState && !/loaded|complete/.test(document.readyState)){setTimeout(_onload, 10);return}
            if (!window.__adroll_loaded){__adroll_loaded=true;setTimeout(_onload, 50);return}
            var scr = document.createElement("script");
            var host = (("https:" == document.location.protocol) ? "https://s.adroll.com" : "http://a.adroll.com");
            scr.setAttribute('async', 'true');
            scr.type = "text/javascript";
            scr.src = host + "/j/roundtrip.js";
            ((document.getElementsByTagName('head') || [null])[0] ||
                document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
        };
        if (window.addEventListener) {window.addEventListener('load', _onload, false);}
        else {window.attachEvent('onload', _onload)}
    }());
	</script>
	<!-- END Adroll-->

	<!-- Hotjar Tracking Code for https://www.seoworks.co.uk/ -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:1522099,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script>

<!-- Click Fraud - Code -->
	<script>
	/*<![CDATA[ */
	var search_params = window.location.search;
	var customer_id = 'MzsrMEA2OTQ0Nw==';
	/* ]]> */
	</script>
	<script type="text/javascript" async src="//clk.anticlickfraudsystem.com/click.js">
	</script>
	<noscript>
	<div style="display:inline;">
	<img height="1" width="1" style="display:none;border-style:none;" alt="" src="//clk.anticlickfraudsystem.com/Click?customer_id=MzsrMEA2OTQ0Nw==" />
	</div>
	</noscript>
<!-- END Click Fraud - Code -->


</body>
</html>
