<?php
// check if the repeater field has rows of data
if( have_rows('header_tracking_codes', 'option') ):
// loop through the rows of data
while ( have_rows('header_tracking_codes','option') ) : the_row();
// display a sub field value

$trackingCode = get_sub_field('header_tracking_code','option');
$post_objects = get_sub_field('header_pages_to_add_tracking_code');
?>
<?php
// print_r ($post_objects);


if( $post_objects ): ?>
  <?php foreach( $post_objects as $post): // variable must be called $post (IMPORTANT) ?>
      <?php setup_postdata($post); ?>
      <?php print_r ($post->ID); ?>
  <?php endforeach; ?>
  <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
<?php endif;



endwhile;
else :
// no rows found
endif;
?>
