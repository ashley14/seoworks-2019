<div class="background-header" id="home">
	<!-- <section class="page-header-content divider-bottom-left" style="background-image: url('https://www.seoworks.co.uk/wp-content/uploads/2015/06/patient-seo.jpg')"> -->
	<header id="header">
	  <nav class="navbar navbar-expand-lg scrolling-navbar nav-bg">

	    <div class="container">
	      <div class="navbar-header">
	        <a class="navbar-brand" href="<?php echo get_home_url(); ?>">
	  				<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/logos/Seoworks-Logo-Color.svg" alt="Seo Works Logo" style="width:250px;">
	  			</a>

	        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-navbar" aria-controls="main-navbar" aria-expanded="false" aria-label="Toggle navigation">
	  				<span class="sr-only">Toggle navigation</span>
	  				<span class="mdi mdi-menu"></span>
	  				<span class="mdi mdi-menu"></span>
	  				<span class="mdi mdi-menu"></span>
	  			</button>
	      </div>
	      <!-- #menu-main-top-menu -->
	  		<?php wp_nav_menu( array( 'theme_location' => 'main_menu', 'container' => 'div', 'container_id' => 'main-navbar', 'container_class' => 'collapse navbar-collapse', 'menu_id' => '', 'menu_class' => 'navbar-nav mr-auto w-100' ) ); ?>
	    </div>

	  </nav>
	</header>



	<!-- Header Content -->
	<?php
	  get_template_part( 'template-parts/homepage/home', 'header');
	?>
	<!-- End Header Content -->

<!-- </div> -->
</div>
