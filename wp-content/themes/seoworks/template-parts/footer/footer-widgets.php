<section class="footer-widgets">
  <div class="container">
    <div class="row justify-content-around">
      <div class="col-sm-12 col-md-6 col-xl-3">
        <aside class="widget">
          <?php dynamic_sidebar( 'footer_1' ); ?>
        </aside>
      </div>

      <div class="col-sm-12 col-md-6 col-xl-3">
        <aside class="widget">
          <?php dynamic_sidebar( 'footer_2' ); ?>
        </aside>
      </div>

      <div class="col-sm-12 col-md-6 col-xl-3">
        <aside class="widget">
          <?php dynamic_sidebar( 'footer_3' ); ?>
        </aside>
      </div>

      <div class="col-sm-12 col-md-6 col-xl-3 ">
        <aside class="widget">
          <?php dynamic_sidebar( 'footer_4' ); ?>
        </aside>
      </div>
    </div>
  </div>
</section>
