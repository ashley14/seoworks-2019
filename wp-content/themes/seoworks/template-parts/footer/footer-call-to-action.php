<section class="footer-callToAction">
  <!-- <div class="sections"> -->
  	<div class="container">
  		<div class="row justify-content-around">

        <div class="col-12 col-md-6 col-xl-4 mb-3">
          <div class="footer-callToAction-content">
            <div class="footer-callToAction-content_icon">
              <svg width="40" height="40" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60;" xml:space="preserve" width="512px" height="512px">
              <path d="M6,2h48c3.252,0,6,2.748,6,6v33c0,3.252-2.748,6-6,6H25.442L15.74,57.673C15.546,57.885,15.276,58,15,58  c-0.121,0-0.243-0.022-0.361-0.067C14.254,57.784,14,57.413,14,57V47H6c-3.252,0-6-2.748-6-6L0,8C0,4.748,2.748,2,6,2z" fill="#FFFFFF"/>
              </svg>
            </div>
            <?php if( have_rows('footer_phone', 'option') ):
               while( have_rows('footer_phone', 'option') ): the_row(); ?>
            <div class="footer-callToAction-content_info">
              <a href="tel:<?php the_sub_field('footer_phone_number'); ?>"><?php the_sub_field('footer_phone_number'); ?></a>
              <p><?php the_sub_field('footer_phone_text'); ?></p>
            </div>
              <?php endwhile;
             endif; ?>
          </div>
        </div>

        <div class="col-12 col-md-6  col-xl-4 mb-3">
          <div class="footer-callToAction-content">
            <div class="footer-callToAction-content_icon">
              <svg width="40" height="40" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 511.626 511.626" style="enable-background:new 0 0 511.626 511.626;" xml:space="preserve">
              <g>
              	<g>
              		<path d="M49.106,178.729c6.472,4.567,25.981,18.131,58.528,40.685c32.548,22.554,57.482,39.92,74.803,52.099    c1.903,1.335,5.946,4.237,12.131,8.71c6.186,4.476,11.326,8.093,15.416,10.852c4.093,2.758,9.041,5.852,14.849,9.277    c5.806,3.422,11.279,5.996,16.418,7.7c5.14,1.718,9.898,2.569,14.275,2.569h0.287h0.288c4.377,0,9.137-0.852,14.277-2.569    c5.137-1.704,10.615-4.281,16.416-7.7c5.804-3.429,10.752-6.52,14.845-9.277c4.093-2.759,9.229-6.376,15.417-10.852    c6.184-4.477,10.232-7.375,12.135-8.71c17.508-12.179,62.051-43.11,133.615-92.79c13.894-9.703,25.502-21.411,34.827-35.116    c9.332-13.699,13.993-28.07,13.993-43.105c0-12.564-4.523-23.319-13.565-32.264c-9.041-8.947-19.749-13.418-32.117-13.418H45.679    c-14.655,0-25.933,4.948-33.832,14.844C3.949,79.562,0,91.934,0,106.779c0,11.991,5.236,24.985,15.703,38.974    C26.169,159.743,37.307,170.736,49.106,178.729z" fill="#FFFFFF"/>
              		<path d="M483.072,209.275c-62.424,42.251-109.824,75.087-142.177,98.501c-10.849,7.991-19.65,14.229-26.409,18.699    c-6.759,4.473-15.748,9.041-26.98,13.702c-11.228,4.668-21.692,6.995-31.401,6.995h-0.291h-0.287    c-9.707,0-20.177-2.327-31.405-6.995c-11.228-4.661-20.223-9.229-26.98-13.702c-6.755-4.47-15.559-10.708-26.407-18.699    c-25.697-18.842-72.995-51.68-141.896-98.501C17.987,202.047,8.375,193.762,0,184.437v226.685c0,12.57,4.471,23.319,13.418,32.265    c8.945,8.949,19.701,13.422,32.264,13.422h420.266c12.56,0,23.315-4.473,32.261-13.422c8.949-8.949,13.418-19.694,13.418-32.265    V184.437C503.441,193.569,493.927,201.854,483.072,209.275z" fill="#FFFFFF"/>
              	</g>
              </svg>
            </div>
            <?php if( have_rows('footer_email', 'option') ):
               while( have_rows('footer_email', 'option') ): the_row(); ?>
            <div class="footer-callToAction-content_info">
              <a href="mailto:<?php the_sub_field('footer_emailaddress'); ?>"><?php the_sub_field('footer_emailaddress'); ?></a>
              <p><?php the_sub_field('footer_email_text'); ?></p>
            </div>
          <?php endwhile;
         endif; ?>
          </div>
        </div>

        <div class="col-12 col-xl-4 mb-3">
          <div class="footer-callToAction-content quote">
            <div class="footer-callToAction-content_icon shake">
              <svg width="40" height="40" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve" width="512px" height="512px">
              <g>
                <g>
                  <path d="M467.812,431.851l-36.629-61.056c-16.917-28.181-25.856-60.459-25.856-93.312V224c0-67.52-45.056-124.629-106.667-143.04    V42.667C298.66,19.136,279.524,0,255.993,0s-42.667,19.136-42.667,42.667V80.96C151.716,99.371,106.66,156.48,106.66,224v53.483    c0,32.853-8.939,65.109-25.835,93.291l-36.629,61.056c-1.984,3.307-2.027,7.403-0.128,10.752c1.899,3.349,5.419,5.419,9.259,5.419    H458.66c3.84,0,7.381-2.069,9.28-5.397C469.839,439.275,469.775,435.136,467.812,431.851z" fill="#FFFFFF"/>
                </g>
              </g>
              <g>
                <g>
                  <path d="M188.815,469.333C200.847,494.464,226.319,512,255.993,512s55.147-17.536,67.179-42.667H188.815z" fill="#FFFFFF"/>
                </g>
              </g>
              </svg>
            </div>
            <?php if( have_rows('footer_request', 'option') ):
               while( have_rows('footer_request', 'option') ): the_row(); ?>
            <div class="footer-callToAction-content_info">
                <?php if( get_sub_field('footer_request_url', 'option') ): ?>
                <a href="<?php the_sub_field('footer_request_url', 'option'); ?>"><?php the_sub_field('footer_request_title'); ?></a>
                <?php else: ?>
                <span class="title"><?php the_sub_field('footer_request_title'); ?></span>
                <?php endif; ?>
              <p><?php the_sub_field('footer_request_text'); ?></p>
            </div>
          <?php endwhile;
         endif; ?>
          </div>
        </div>

  		</div>
  	</div>
  <!-- </div> -->
</section>
