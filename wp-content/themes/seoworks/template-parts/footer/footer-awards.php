<section class="footer-awards">
  <div class="sections-tb">
  	<div class="container">
  		<div class="row justify-content-around">
        <div class="col-12">
          <?php the_field('footer_awards_content', 'option'); ?>
        </div>
  		</div>
  	</div>
  </div>
</section>
