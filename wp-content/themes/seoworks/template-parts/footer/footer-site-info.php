<div class="footer-site-info site-info">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="site-info_menu">
          <nav>
            <?php wp_nav_menu( array( 'theme_location' => 'footer_menu' ) ); ?>
          </nav>
        </div>
        <div class="site-info_copyright copyright">
          <p>Copyright <?php echo date('Y'); ?> <?php the_field('footer_copyright', 'option'); ?></p>
        </div>
      </div>
    </div>
  </div>
</div><!-- .site-info -->
