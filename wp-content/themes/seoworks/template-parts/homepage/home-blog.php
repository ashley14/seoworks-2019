<?php
if( have_rows('homepage_block_5') ):
  while ( have_rows('homepage_block_5') ) : the_row();
?>
<section class="home-blog divider-top-right" style="background-image: url('<?php the_sub_field('homepage_block_5_bg'); ?>')">

  <div class="sections">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="title">
            <h2><?php the_sub_field('homepage_block_5_title'); ?></h2>
          </div>
          <?php the_sub_field('homepage_block_5_content'); ?>
        </div>
      </div>
    </div>

    <div class="container">
      <div class="row">
        <?php
        // Query Arguments
          $args = array(
          'post_type' => array('post'),
          'post_status' => array('publish'),
          'posts_per_page' => 3,
          'order' => 'DESC',
          'orderby' => 'date',
          );

          // The Query
          $blog_post = new WP_Query( $args );

          // The Loop
          if ( $blog_post->have_posts() ) {
          while ( $blog_post->have_posts() ) {
            $blog_post->the_post(); ?>

            <div class="col-sm-6 col-xl-4">
              <div class="card">
                <div class="card-image">
                  <figure>
                    <a href="<?php the_permalink(); ?>">
                      <?php the_post_thumbnail('thumb-blog'); ?>
                    </a>
                  </figure>
                </div>
                <div class="card-body">
                  <p class="card-date">Date posted: <span><?php the_date(); ?></span></p>
                  <h5 class="card-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                  <div class="card-text"><?php the_excerpt(); ?></div>
                  <a href="<?php the_permalink(); ?>" class="btn btn-readmore_blog">Read More</a>
                </div>
              </div>
            </div>

        <?php  }
          } else {
          // no posts found
          }
          /* Restore original Post Data */
          wp_reset_postdata();
        ?>

      </div>
    </div>
  </div>
</section>
<?php
endwhile;
  else :
endif;
?>
