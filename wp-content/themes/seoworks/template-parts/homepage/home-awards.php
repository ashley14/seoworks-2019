<section class="home-awards text-center">
  <div class="sections">
    <div class="container">
      <?php
			if( have_rows('homepage_block_4') ):
				while ( have_rows('homepage_block_4') ) : the_row();
			?>
      <div class="row">
        <div class="col-12">
          <div class="title">
            <h2><?php the_sub_field('homepage_block_4_title'); ?></h2>
          </div>
          <?php the_sub_field('homepage_block_4_content'); ?>
        </div>
      </div>
      <?php
			endwhile;
				else :
			endif;
			?>
    </div>
  </div>

  <?php
  // Query Arguments
    $args = array(
    'post_type' => array('award'),
    'posts_per_page' => -1,
    'order' => 'DESC',
    'orderby' => 'date',
    );
    // The Query
    $slider_awards = new WP_Query( $args ); ?>

  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="owl-carousel carousel-awards">

          <?php // The Loop
            if ( $slider_awards->have_posts() ) {
              while ( $slider_awards->have_posts() ) {
                $slider_awards->the_post();
                if ( has_post_thumbnail() ) { ?>
                  <div class="carousel-award">
                    <figure>
                      <?php the_post_thumbnail( 'thumb-sliders' ); ?>
                    </figure>
                  </div>
                  <?php
                }
              }
            } else {
              // no posts found
              }
            /* Restore original Post Data */
            wp_reset_postdata();
        ?>

        </div>
        </div>
      </div>
    </div>
  </div>

</section>
