<section id="sectors" class="home-sectors text-center">
  <div class="sections">
  	<div class="container">
      <?php
			if( have_rows('homepage_block_3') ):
				while ( have_rows('homepage_block_3') ) : the_row();
			?>
  		<div class="row">
        <div class="col-12">
          <div class="title">
            <h2><?php the_sub_field('homepage_block_3_title'); ?></h2>
  				</div>
          <?php the_sub_field('homepage_block_3_content'); ?>
        </div>
    	</div>
      <?php
			endwhile;
				else :
			endif;
			?>
  	</div>
  </div>
</section>
