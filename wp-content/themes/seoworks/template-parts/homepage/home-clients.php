<section id="clients" class="home-clients"  data-scroll-index="1">
  <div class="sections">
    <div class="container">
      <?php
			if( have_rows('homepage_block_1') ):
				while ( have_rows('homepage_block_1') ) : the_row();
			?>
    	<div class="row">
    		<div class="col-12">
          <div class="title">
            <h2><?php the_sub_field('homepage_block_1_title'); ?></h2>

            <?php the_sub_field('homepage_block_1_content'); ?>
          </div>
    		</div>
    	</div>
      <?php
			endwhile;
				else :
			endif;
			?>
    </div>

    <?php
    // Query Arguments
      $args = array(
      'post_type' => array('client'),
      'posts_per_page' => -1,
      'order' => 'DESC',
      'orderby' => 'date',
      );
      // The Query
      $slider_clients = new WP_Query( $args ); ?>

    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="owl-carousel carousel-clients">

            <?php // The Loop
              if ( $slider_clients->have_posts() ) {
                while ( $slider_clients->have_posts() ) {
                  $slider_clients->the_post();
                  if ( has_post_thumbnail() ) { ?>
                    <div class="carousel-client">
                      <figure>
                        <?php the_post_thumbnail( 'thumb-sliders' ); ?>
                      </figure>
                    </div>
                    <?php
                  }
                }
              } else {
                // no posts found
                }
              /* Restore original Post Data */
              wp_reset_postdata();
          ?>

          </div>
          </div>
        </div>
      </div>
    </div>

</section>
