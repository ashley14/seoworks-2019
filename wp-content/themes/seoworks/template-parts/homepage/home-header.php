<div class="main-slide" style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/homepage-header.jpg); width: 100%; height:100%;">
  <div class="main-slide_content" style="margin: 0 auto;">
    <?php // $sliderAlias = get_field('homepage_slider'); ?>
    <?php // putRevSlider("$sliderAlias") ?>
    <div class="main-slide_logo">
      <img class="data-lazyloaded='1'" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/logos/Seoworks-Logo-White.svg" alt="Seo Works">
    </div>
    <div class="main-slide_body">
      <h2 class="main-slide_title" style="color:white;">Get More Customers Online</h2>
      <h3 class="main-slide_sub">Multiple <span>award-winning</span> search marketing with <br> outstanding results.</h3>
      <div class="main-slide_logos">
        <ul>
          <li><img class="data-lazyloaded='1'" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/awards/drum-search-winner.png" alt="Drum Search Winner"></li>
          <li><img class="data-lazyloaded='1'" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/awards/Uk-search-awards.png" alt="Uk search Awards"></li>
          <li><img class="data-lazyloaded='1'" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/awards/rarlogo.png" alt="RAR+ Recommended"></li>
          <li><img class="data-lazyloaded='1'" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/awards/PNA18.png" alt="PNA18"></li>
        </ul>
      </div>
    </div>

    <div class="main-slide_btn">
      <a href="/about-seoworks/">About Us</a>
      <a href="/request-a-free-seo-analysis/">Free Audit</a>
    </div>

  </div>
</div>
