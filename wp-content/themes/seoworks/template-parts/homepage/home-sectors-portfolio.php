<section id="portfolio" class="home-sectors-portfolio">
	<div class="sections">
		<div class="container">
			<div class="row">
				<div class="col-12">
		      <div class="title">
						<h3>Our Key Sectors</h3>
					</div>
					<div class="home-sectors-portfolio_filter">
						<?php
						// your taxonomy name
							$tax = 'sector';
							// get the terms of taxonomy
							$terms = get_terms( $tax, $args = array(
								'hide_empty' => true, // do not hide empty terms
							));
							echo '<a href="'. get_home_url() .'/search-engine-optimisation-testimonials/'.'">All</a>';
					    if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
					        // loop through all terms
					        foreach( $terms as $term ) {
					            // Get the term link
					            $term_link = get_term_link( $term );
					            if( $term->count > 0 )
					               // display term name
												// echo '<button type="button" data-filter="' . '.' . $term->slug . '"> ' . $term->name . '</button>';
												echo '<a href="'. get_home_url() .'/sector/'. $term->slug .'"> ' . $term->name . '</a>';

					            // elseif( $term->count !== 0 )
					            //     // display name
					            //     echo '' . $term->name .'';
					        }
					    }
						?>
					</div>
				</div>
			</div>
	  </div>
	 </div>

  <div class="sectors sector-list">

		<?php

		// Query Arguments
		$args = array(
		'post_type' => array('case-studies'),
		'meta_query' => array(
			// Showing only services page that have the option in yes to show on the homepage
			array(
				'key'	 	=> 'show_casestudy_homepage',
				'value'	  	=> 'yes')
		),
		'posts_per_page' => 6,
		'order' => 'DESC',
		);

		// The Query
		$case_studies = new WP_Query( $args );

		// The Loop
		if ( $case_studies->have_posts() ) {
		while ( $case_studies->have_posts() ) {
			$case_studies->the_post();
			$getslugid = wp_get_post_terms( $post->ID, 'sector' );
			$slugs = implode(' ',wp_list_pluck($getslugid,'slug'));
		?>

			<article class="sector mix <?php echo $slugs; ?>" style="background-image:url('<?php the_field('case_study_header_background_image'); ?>')">
				<div class="sector-content">
					<div class="container">
						<div class="row">
							<div class="col-xl-6">
								<div class="sector-content_description">
									<h4><?php the_title(); ?></h4>
									<p><?php the_content(); ?></p>
									<a href="<?php the_permalink(); ?>" class="btn btn-readmore_portfolio">Read Case Study</a>
								</div>
							</div>

							<div class="col-xl-6 align-self-center">
								<div class="sector-content_logo">
									<figure>
										<?php the_post_thumbnail( 'case-study-logo' ); ?>
									</figure>
								</div>
							</div>
						</div>
					</div>
				</div>
			</article>
		<?php }
		} else {
		// no posts found
		}
		/* Restore original Post Data */
		wp_reset_postdata();
		?>



    <!-- <article class="sector mix health education ecommerce-seo" style="background-image:url('<?php //echo get_stylesheet_directory_uri(); ?>/assets/images/sectors/sector-2.jpg')">
      <div class="sector-content">
        <div class="container">
          <div class="row">
						<div class="col-xl-6">
              <div class="sector-content_description">
                <h4>Finalcil Service SEO for St. James's Plave Wealth Managent</h4>
                <p>Improving ranking, and leads generation for a FTSE 100 finalcial services provider</p>
                <a href="#" class="btn btn-readmore_portfolio">Read Case Study</a>
              </div>
            </div>

						<div class="col-xl-6 align-self-center">
              <div class="sector-content_logo">
                <figure>
                  <img src="<?php // echo get_stylesheet_directory_uri(); ?>/assets/images/sectors/logos/Lola-Rose_b.png" alt="">
                </figure>
              </div>
            </div>
          </div>
        </div>
      </div>
    </article> -->

    <!-- <article class="sector mix health" style="background-image:url('<?php // echo get_stylesheet_directory_uri(); ?>/assets/images/sectors/sector-3.jpg')">
      <div class="sector-content">
        <div class="container">
          <div class="row">
						<div class="col-xl-6">
              <div class="sector-content_description">
                <h4>Finalcil Service SEO for St. James's Plave Wealth Managent</h4>
                <p>Improving ranking, and leads generation for a FTSE 100 finalcial services provider</p>
                <a href="#" class="btn btn-readmore_portfolio">Read Case Study</a>
              </div>
            </div>

						<div class="col-xl-6 align-self-center">
              <div class="sector-content_logo">
                <figure>
                  <img src="<?php // echo get_stylesheet_directory_uri(); ?>/assets/images/sectors/logos/Hallam_b.png" alt="">
                </figure>
              </div>
            </div>
          </div>
        </div>
      </div>
    </article> -->

    <!-- <article class="sector mix education" style="background-image:url('<?php // echo get_stylesheet_directory_uri(); ?>/assets/images/sectors/sector-1.jpg')">
      <div class="sector-content">
        <div class="container">
          <div class="row">
						<div class="col-xl-6">
              <div class="sector-content_description">
                <h4>Finalcil Service SEO for St. James's Plave Wealth Managent</h4>
                <p>Improving ranking, and leads generation for a FTSE 100 finalcial services provider</p>
                <a href="#" class="btn btn-readmore_portfolio">Read Case Study</a>

              </div>
            </div>

						<div class="col-xl-6 align-self-center">
              <div class="sector-content_logo">
                <figure>
                  <img src="<?php // echo get_stylesheet_directory_uri(); ?>/assets/images/sectors/logos/Dainesse_b.png" alt="">
                </figure>
              </div>
            </div>
          </div>
        </div>
      </div>
    </article> -->

  </div>
</section>
