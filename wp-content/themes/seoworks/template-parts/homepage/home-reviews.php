<section class="home-reviews text-center" style="background-image: url('<?php the_field('homepage_testimonials_bg'); ?>')">
  <div class="sections">
    <?php
    // Query Arguments
      $args = array(
      'post_type' => array('review'),
      'posts_per_page' => -8,
      'order' => 'DESC',
      'orderby' => 'date',
      );
      // The Query
      $slider_reviews = new WP_Query( $args ); ?>
  	<div class="container">
  		<div class="row">
        <div class="owl-carousel carousel-reviews">
          <?php // The Loop
            if ( $slider_reviews->have_posts() ) {
              while ( $slider_reviews->have_posts() ) {
                $slider_reviews->the_post(); ?>
                  <blockquote class="blockquote">
                    <p class="mb-0"><?php the_content(); ?></p>
                    <footer class="blockquote-footer"><?php the_title(); ?> <!--<cite title="Source Title">Source Title</cite>--></footer>
                    <div class="quality">
                      <svg width="30px" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                      	 viewBox="0 0 426.667 426.667" style="enable-background:new 0 0 426.667 426.667;" xml:space="preserve">
                      <polygon style="fill:#FAC917;" points="213.333,10.441 279.249,144.017 426.667,165.436 320,269.41 345.173,416.226 213.333,346.91
                      	81.485,416.226 106.667,269.41 0,165.436 147.409,144.017 "/>
                      </svg>
                      <svg width="30px" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 426.667 426.667" style="enable-background:new 0 0 426.667 426.667;" xml:space="preserve">
                      <polygon style="fill:#FAC917;" points="213.333,10.441 279.249,144.017 426.667,165.436 320,269.41 345.173,416.226 213.333,346.91
                        81.485,416.226 106.667,269.41 0,165.436 147.409,144.017 "/>
                      </svg>
                      <svg width="30px" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 426.667 426.667" style="enable-background:new 0 0 426.667 426.667;" xml:space="preserve">
                      <polygon style="fill:#FAC917;" points="213.333,10.441 279.249,144.017 426.667,165.436 320,269.41 345.173,416.226 213.333,346.91
                        81.485,416.226 106.667,269.41 0,165.436 147.409,144.017 "/>
                      </svg>
                      <svg width="30px" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 426.667 426.667" style="enable-background:new 0 0 426.667 426.667;" xml:space="preserve">
                      <polygon style="fill:#FAC917;" points="213.333,10.441 279.249,144.017 426.667,165.436 320,269.41 345.173,416.226 213.333,346.91
                        81.485,416.226 106.667,269.41 0,165.436 147.409,144.017 "/>
                      </svg>
                      <svg width="30px" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 426.667 426.667" style="enable-background:new 0 0 426.667 426.667;" xml:space="preserve">
                      <polygon style="fill:#FAC917;" points="213.333,10.441 279.249,144.017 426.667,165.436 320,269.41 345.173,416.226 213.333,346.91
                        81.485,416.226 106.667,269.41 0,165.436 147.409,144.017 "/>
                      </svg>
                    </div>
                  </blockquote>
                  <?php
              }
            } else {
              // no posts found
              }
            /* Restore original Post Data */
            wp_reset_postdata();
          ?>
        </div>
  		</div>
  	</div>
  </div>
</section>
