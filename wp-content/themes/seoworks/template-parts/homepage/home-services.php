<section id="services" class="home-services text-center">
  <div class="sections">
  	<div class="container">
      <?php
			if( have_rows('homepage_block_2') ):
				while ( have_rows('homepage_block_2') ) : the_row();
			?>
  		<div class="row">
        <div class="col-12">
          <div class="title">
            <h1><?php the_sub_field('homepage_block_2_title'); ?></h1>
            <?php the_sub_field('homepage_block_2_content'); ?>
  				</div>
        </div>
  		</div>
      <?php
			endwhile;
				else :
			endif;
			?>
  	</div>
  </div>

  <div class="sections">
    <div class="home-services-grid divider-top-left divider-bottom-right" style="background: url('<?php the_field('homepage_services_bg'); ?>')">
      <div class="overlayed">
        <div class="container">
          <div class="row no-gutters">
            <?php
            // Query Arguments
              $args = array (
                'post_type' => 'page',
                'meta_query' => array(
                  // selecting pages which has 'service' as page template
                  array (
                    'key'   => '_wp_page_template',
                    'value' => array(
                      'templates/template-service.php',
                      'templates/template-service-v2.php'
                    )
                  ),
                  // Showing only services page that have the option in yes to show on the homepage
                  array(
              			'key'	 	=> 'show_service_homepage',
              			'value'	  	=> 'yes')
                ),
                'post_status' => array('publish'),
                'posts_per_page' => 6,
                'order' => 'DESC',
                'orderby' => 'date',
              );

              // The Query
              $services = new WP_Query( $args );

              // The Loop
              if ( $services->have_posts() ) {
              while ( $services->have_posts() ) {
                $services->the_post();
                $featured_service_img_url = get_the_post_thumbnail_url(get_the_ID(),'thumb-services');?>

                <div class="col-sm-6 col-md-6 col-lg-4">
                  <div class="item service" style="background-image:url('<?php echo esc_url($featured_service_img_url); ?>')">
                    <!-- SERVICE TITLE -->
                    <h3><?php the_title() ?></h3>
                    <!-- HOVER EFFECT -->
                    <a href="<?php the_permalink(); ?>" class="service-hover__link">
                      <div class="service-hover">
                        <!-- HOVER SERVICES TITLE -->
                        <h5><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></h5>
                        <!-- HOVER SERVICES CONTENT -->
                        <?php if( get_field('service_custom_excerpt') ): ?>
                          <?php the_field('service_custom_excerpt'); ?></p>
                        <?php else: ?>
                        <?php the_excerpt(); ?>
                        <?php endif; ?>
                      </div>
                    </a>
                  </div>
                </div>

              <?php
              }
              } else {
              // no posts found
              }
              /* Restore original Post Data */
              wp_reset_postdata();
            ?>

          </div>
        </div>

      </div>
    </div>
  </div>
</section>
