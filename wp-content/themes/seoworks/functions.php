<?php
/**
 * SeoWorks functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package SeoWorks
 */

if ( ! function_exists( 'seoworks_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function seoworks_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on SeoWorks, use a find and replace
		 * to change 'seoworks' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'seoworks', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );


		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'main_menu' => esc_html__( 'Primary Main Menu', 'seoworks.co.uk' ),
			'footer_menu' => esc_html__( 'Footer Menu', 'seoworks.co.uk' ),

		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'seoworks_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;



add_action( 'after_setup_theme', 'seoworks_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function seoworks_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'seoworks_content_width', 640 );
}
add_action( 'after_setup_theme', 'seoworks_content_width', 0 );

// Adding async to all scripts
function add_async_forscript($url)
{
    if (strpos($url, '#asyncload')===false)
        return $url;
    else if (is_admin())
        return str_replace('#asyncload', '', $url);
    else
        return str_replace('#asyncload', '', $url)."' async='async";
}
add_filter('clean_url', 'add_async_forscript', 11, 1);


/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function seoworks_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'seoworks' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'seoworks' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}


// Register custom sidebars
function footer_widget() {

		$args = array(
			'name'          => __( 'Footer 1', 'seoworks.co.uk' ),
			'description'   => __( 'Footer Widget', 'seoworks.co.uk' ),
			'id'            => 'footer_1',
			'before_widget' => '<div id="%1$s" class="widget-content %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h6 class="widget-title">',
			'after_title'   => '</h6>',
		);
		register_sidebar($args);

		$args = array(
			'name'          => __( 'Footer 2', 'seoworks.co.uk' ),
			'description'   => __( 'Footer Widget', 'seoworks.co.uk' ),
			'id'            => 'footer_2',
			'before_widget' => '<div id="%1$s" class="widget-content %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h6 class="widget-title">',
			'after_title'   => '</h6>',
		);
		register_sidebar($args);

		$args = array(
			'name'          => __( 'Footer 3', 'seoworks.co.uk' ),
			'description'   => __( 'Footer Widget', 'seoworks.co.uk' ),
			'id'            => 'footer_3',
			'before_widget' => '<div id="%1$s" class="widget-content %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h6 class="widget-title">',
			'after_title'   => '</h6>',
		);
		register_sidebar($args);

		$args = array(
			'name'          => __( 'Footer 4', 'seoworks.co.uk' ),
			'description'   => __( 'Footer Widget', 'seoworks.co.uk' ),
			'id'            => 'footer_4',
			'before_widget' => '<div id="%1$s" class="widget-content %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h6 class="widget-title">',
			'after_title'   => '</h6>',
		);
		register_sidebar($args);

}
add_action( 'widgets_init', 'footer_widget' );

/**
 * Setting up images size.
 */
add_action( 'after_setup_theme', 'img_size_theme' );
function img_size_theme() {
		add_image_size( 'thumb-sliders', 200 ); // 200px wide auto height and cropped
		add_image_size( 'thumb-services', 400, 400, true ); // 400px wide 400px height and cropped
		add_image_size( 'thumb-blog', 350, 235, true ); // 400px wide 400px height and cropped
		add_image_size( 'page-header', 1920, 350 ); // 1920 wide 300 height Page header
		add_image_size( 'case-study-logo', 350 ); // 350 wide auto height Case study logo



		// add_image_size( 'homepage-thumb', 220, 180, true ); // (cropped)
}

add_action( 'widgets_init', 'seoworks_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function seoworks_scripts() {

	wp_enqueue_style( 'seoworks-style', get_template_directory_uri() . '/css/style.min.css');

	// owl carousel CSS
	wp_enqueue_style( 'owl-carousel-style', get_template_directory_uri() . '/inc/plugins/OwlCarousel/owl.carousel.min.css');

	wp_enqueue_script( 'seoworks-navigation', get_template_directory_uri() . '/js/navigation.js#asyncload', array(), '20151215', true );

	wp_enqueue_script( 'seoworks-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js#asyncload', array(), '20151215', true );

	// owl carousel JS
	wp_enqueue_script( 'owl-carousel-js', get_template_directory_uri() . '/inc/plugins/OwlCarousel/owl.carousel.min.js#asyncload', array(), '2.3.4', true );

	// Bootstrap JS
 	wp_enqueue_script( 'bootstrap-js', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js#asyncload', array( 'jquery' ) );

	// MixitUp JS only for the case studies archive page
	if (is_post_type_archive(array('case-studies')) ) {
		wp_enqueue_script( 'mixitup-js', get_template_directory_uri() . '/inc/plugins/mixitup/mixitup.min.js', array(), '3.3.0', true );
	}

	// Main JS
	wp_enqueue_script( 'main-js', get_template_directory_uri() . '/js/main.js#asyncload', array(), '1.0', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'seoworks_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Loading shortcodes
 */
 require get_template_directory() . '/inc/shortcodes/shortcodes.php';

/**
 * Loading custom post types
 */
 require get_template_directory() . '/inc/cpt/custom-post-types.php';

 /**
  * Loading Custom Fields using Plugin ACF
  */
  require get_template_directory() . '/inc/acf/avanced-custom-fields.php';


// Removing Read more from the excerpt
 function new_excerpt_more( $more ) {
     return '...';
 }
 add_filter('excerpt_more', 'new_excerpt_more');

 // Google Maps API for acf
 function my_acf_init() {
	acf_update_setting('google_api_key', 'xxx');
}
add_action('acf/init', 'my_acf_init');

//* Removing label on Gravity forms
add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );

// Numerical pagination
function pagination($pages = '', $range = 4)
{
     $showitems = ($range * 2)+1;

     global $paged;
     if(empty($paged)) $paged = 1;

     if($pages == '')
     {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
     }

     if(1 != $pages)
     {
         echo "<div class=\"pagination\"><span class='total-pages'>Page ".$paged." of ".$pages."</span>";
					 echo "<div class=\"pagination-pages\">";
					 if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."' class='page-first'>&laquo; First</a>";
					 // if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'>&lsaquo;</a>";
	         for ($i=1; $i <= $pages; $i++)
	         {
	             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
	             {
	                 echo ($paged == $i)? "<span class=\"current\">".$i."</span>":"<a href='".get_pagenum_link($i)."' class=\"inactive\">".$i."</a>";
	             }
	         }

	         // if ($paged < $pages && $showitems < $pages) echo "<a href=\"".get_pagenum_link($paged + 1)."\">&rsaquo;</a>";
	         if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."' class='page-last'>Last &raquo;</a>";
					 echo "</div>\n";
				 echo "</div>\n";
     }
}

// Registering Custom Thumbnails for the Case studies
if (class_exists('MultiPostThumbnails')) {
    new MultiPostThumbnails(
        array(
            'label' => __( 'Company Logo (color)', 'seoworks.co.uk'),
            'id' => 'second-image-casestudy',
            'post_type' => 'case-studies'
        )
    );
}


// Custom wp-admin login
function seoworks_login_logo() { ?>
    <style type="text/css">
				body {
					background: #303030!important;
				}
        #login h1 a, .login h1 a {

          background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/logos/Seoworks-Logo-White.svg);
          /* height:65px; */
          width:320px;
          background-size: 320px 90px;
          background-repeat: no-repeat;
          /* padding-bottom: 30px; */
        }
				p#nav,p#backtoblog {
					text-align: center!important;
				}
					p#nav a,
					p#backtoblog a {
						color: #fff!important;
					}
						p#nav a:hover,
						p#backtoblog a:hover {
							color: #85BD3C!important;
						}
				.wp-core-ui .button-primary:hover {
					background: #85BD3C!important;
					border-color: #85BD3C!important;
					text-shadow: 0 -1px #85BD3C;
					box-shadow: 0 1px 0 #85BD3C;
				}
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'seoworks_login_logo' );

function seoworks_login_logo_url() {
    return home_url();
}
add_filter( 'login_headerurl', 'seoworks_login_logo_url' );

function seoworks_login_logo_url_title() {
    return 'Award-Winning UK SEO Company - The SEO Works';
}
add_filter( 'login_headertitle', 'seoworks_login_logo_url_title' );

// show post thumbnails in feeds
function seoworks_post_thumbnail_feeds($content) {
	global $post;
	if(has_post_thumbnail($post->ID)) {
		$content = get_the_post_thumbnail($post->ID) . $content;
	}
	return $content;
}
add_filter('the_excerpt_rss', 'seoworks_post_thumbnail_feeds');
add_filter('the_content_feed', 'seoworks_post_thumbnail_feeds');
