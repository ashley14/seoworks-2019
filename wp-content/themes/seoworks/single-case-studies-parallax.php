<?php
/*
* Template Name: Parallax
* Template Post Type: case-studies
*/
?>

<?php
  get_header();
  $term_name = get_queried_object()->name;
  $post_type = get_post_type();
  $post_id = get_the_ID();



?>

<?php
  while ( have_posts() ) :
    the_post();
    // vars
    $imageOptions = get_field('parallax_images', $post_id);
    $backgroundMobile = $imageOptions['parallax_image_mobile_tablet'];
    $backgroundDesktop = $imageOptions['parallax_image_desktop'];
?>

<section class="slide">
  <article class="slide_content">
    <figure class="background-mobile">
      <img src="<?php echo $backgroundMobile; ?>" alt="">
    </figure>

    <div class="slide_content-text">
      <div class="slide_content-info">
        <div class="title">
          <h1><?php echo the_title(); ?></h1>
        </div>
        <p><?php echo the_content(); ?></p>
      </div>
      <div class="slide_content-challenge">
        <h2>Challenge</h2>
        <?php the_field('case_study_challenge'); ?>
      </div>
      <div class="slide_content-solution">
        <h2>Solution</h2>
        <?php the_field('case_study_solution'); ?>
      </div>
      <div class="slide_content-result">
        <h2>Result</h2>
        <?php the_field('case_study_result'); ?>
      </div>
      <a class="btn btn-green" href="<?php echo get_home_url(); ?>/portfolio/">View our Portfolio</a>
    </div>
    <div class="background" style="background: url(<?php echo $backgroundDesktop; ?>)"></div>
  </article>
</section>

<?php
  endwhile; // End of the loop.
?>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/TweenMax.min.js"></script>
		 <script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/ScrollMagic.min.js"></script>
		 <script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/plugins/animation.gsap.min.js"></script>
		 <script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/plugins/debug.addIndicators.min.js"></script>

     <script type="text/javascript">
     $(function () { // wait for document ready
		// init
		var controller = new ScrollMagic.Controller({
			globalSceneOptions: {
				triggerHook: 'onLeave',
				duration: "0" // this works just fine with duration 0 as well
				// However with large numbers (>20) of pinned sections display errors can occur so every section should be unpinned once it's covered by the next section.
				// Normally 100% would work for this, but here 200% is used, as  3 is shown for more than 100% of scrollheight due to the pause.
			}
		});

		// get all slides
		var slides = document.querySelectorAll("section.slide");

		// create scene for every slide
		for (var i=0; i<slides.length; i++) {
			new ScrollMagic.Scene({
					triggerElement: slides[i]
				})
				.setPin(slides[i], {pushFollowers: false})
				// .addIndicators() // add indicators (requires plugin)
				.addTo(controller);
        if ($( window ).width() <= 768) {
          controller.enabled(false);
        }
		}
	});

     </script>
<?php
  get_footer();
?>
