<?php
/**
 * The template for displaying all Casy studies pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package SeoWorks
 */

get_header(); ?>
<div class="case-study">
  <div class="header-content divider-bottom-left" style="background-image: url('<?php the_field('case_study_header_background_image'); ?>')">
    <div class="container">

      <div class="row">
        <div class="col-md-4 mx-auto">
          <figure>
            <?php the_post_thumbnail( 'case-study-logo' ); ?>
          </figure>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12 mx-auto mt-3">
          <?php
            while ( have_posts() ) :
              the_post();
              the_content();
            endwhile; // End of the loop.
          ?>
        </div>
      </div>

    </div>
  </div>


  <div id="primary" class="content-area">
    <main id="main" class="site-main">

      <?php
        while ( have_posts() ) :
          the_post();
      ?>

      <?php
        $terms = get_the_terms( $post->ID , 'sector' );
        if ( $terms != null ){
         foreach( $terms as $term ) {
         // Print the name method from $term which is an OBJECT
         $catName = $term->name;
         $catDescription = $term->description;
         // Get rid of the other data stored in the object, since it's not needed
         unset($term);
        } }
      ?>
      <div class="content">
        <section class="case-study_information">
          <div class="container">
            <div class="row">
              <div class="col-12">
                <div class="title">
                  <h1><?php echo the_title(); //$catName ?></h1>
                  <p><?php echo $catDescription ?></p>
                  <a class="pulse free-seo-analysis" href="/contact-us/">
                    <span>
                      <svg version="1.1" id="Layer_1" width="40px" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                      	 viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                      	<style type="text/css">
                      		.st0{fill:#85BD3C;}
                      	</style>
                      	<g>
                      		<g>
                      			<path class="st0" d="M457.6,134.7c-30,0-54.4,24.4-54.4,54.4c0,10.9,3.3,21.1,8.8,29.6l-58.7,58.7c-8.5-5.6-18.7-8.8-29.6-8.8
                      				c-11.3,0-21.9,3.5-30.6,9.4l-59.3-59.3c5.6-8.5,8.8-18.7,8.8-29.6c0-30-24.4-54.4-54.4-54.4s-54.4,24.4-54.4,54.4
                      				c0,10.9,3.3,21.1,8.8,29.6L84,277.4c-8.5-5.6-18.7-8.8-29.6-8.8C24.4,268.6,0,293,0,323s24.4,54.4,54.4,54.4s54.4-24.4,54.4-54.4
                      				c0-10.9-3.3-21.1-8.8-29.6l58.7-58.7c8.5,5.6,18.7,8.8,29.6,8.8c10.9,0,21.1-3.3,29.6-8.8l59.7,59.7c-5.2,8.3-8.2,18.2-8.2,28.7
                      				c0,30,24.4,54.4,54.4,54.4s54.4-24.4,54.4-54.4c0-10.9-3.3-21.1-8.8-29.6l58.7-58.7c8.5,5.6,18.7,8.8,29.6,8.8
                      				c30,0,54.4-24.4,54.4-54.4C512,159.1,487.6,134.7,457.6,134.7z M54.4,354.8c-17.6,0-31.9-14.3-31.9-31.9
                      				c0-17.6,14.3-31.9,31.9-31.9c17.6,0,31.9,14.3,31.9,31.9C86.3,340.5,71.9,354.8,54.4,354.8z M188.3,220.9
                      				c-17.6,0-31.9-14.3-31.9-31.9c0-17.6,14.3-31.9,31.9-31.9s31.9,14.3,31.9,31.9S205.9,220.9,188.3,220.9z M323.7,354.8
                      				c-17.6,0-31.9-14.3-31.9-31.9s14.3-31.9,31.9-31.9c17.6,0,31.9,14.3,31.9,31.9C355.6,340.5,341.3,354.8,323.7,354.8z M457.6,220.9
                      				c-17.6,0-31.9-14.3-31.9-31.9c0-17.6,14.3-31.9,31.9-31.9c17.6,0,31.9,14.3,31.9,31.9S475.2,220.9,457.6,220.9z"/>
                      		</g>
                      	</g>
                      	<g>
                      		<g>
                      			<path class="st0" d="M176.8,272.7c-4.4-4.4-11.5-4.4-15.9,0L138,295.6c-4.4,4.4-4.4,11.5,0,15.9c2.2,2.2,5.1,3.3,7.9,3.3
                      				s5.8-1.1,7.9-3.3l22.9-22.9C181.2,284.2,181.2,277.1,176.8,272.7z"/>
                      		</g>
                      	</g>
                      	<g>
                      		<g>
                      			<path class="st0" d="M374,200.9c-4.4-4.4-11.5-4.4-15.9,0l-22.9,22.9c-4.4,4.4-4.4,11.5,0,15.9c2.2,2.2,5.1,3.3,7.9,3.3
                      				c2.9,0,5.8-1.1,7.9-3.3l22.9-22.9C378.3,212.4,378.3,205.3,374,200.9z"/>
                      		</g>
                      	</g>
                      </svg>
                    </span>
                    Start your next project - request a free quote today
                </a>
                </div>
              </div>
            </div>
          </div>
        </section>

        <section class="case-study_overview">
          <div class="container">
            <div class="row">
              <?php
              // check if the repeater field has rows of data
              if( have_rows('case_study_overviews') ):
              // loop through the rows of data
                while ( have_rows('case_study_overviews') ) : the_row(); ?>
                <div class="col-md-4 mx-auto mb-3">
                    <!-- display a sub field value -->
                  <div class="overview">
                    <div class="overview-icon">
                      <img src="<?php the_sub_field('case_study_overview_icon'); ?>">
                    </div>
                    <p><?php the_sub_field('case_study_overview'); ?></p>
                  </div>
                </div>
              <?php  endwhile;
              else :
                // no rows found
              endif;
              ?>
            </div>
          </div>
        </section>

        <section class="case-study_challenge">
          <div class="container">
            <div class="row">
              <div class="col-12">
                <div class="title">
                  <h2>Challenge</h2>
                </div>
                <?php the_field('case_study_challenge'); ?>
              </div>
            </div>
          </div>
        </section>

        <section class="case-study_solution" style="background-image: url('<?php the_field('case_study_header_background_image'); ?>')">
          <div class="container">
            <div class="row">
              <div class="col-12">
                <div class="title">
                  <h2>Solution</h2>
                </div>
                <?php the_field('case_study_solution'); ?>
              </div>
            </div>
          </div>
        </section>

        <section class="case-study_result">
          <div class="container">
            <div class="row">
              <div class="col-12">
                <div class="title">
                  <h2>Result</h2>
                </div>
                <?php the_field('case_study_result'); ?>
              </div>
            </div>
          </div>
        </section>




      <?php
        endwhile; // End of the loop.
      ?>
      </div>

    </main><!-- #main -->
  </div><!-- #primary -->
</div>


<?php get_footer(); ?>
