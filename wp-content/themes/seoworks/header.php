<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package SeoWorks
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel='icon' href='<?php echo get_stylesheet_directory_uri(); ?>/assets/images/fav.png' type='image/x-icon'/ >
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php wp_head(); ?>
	<?php // get_template_part( 'template-parts/header/header', 'tracking'); ?>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'seoworks' ); ?></a>

	<?php
		if ( is_front_page() || is_home() ) :
			get_template_part( 'template-parts/header/header', 'home');
		else :
			get_template_part( 'template-parts/header/header', 'inner');
		endif;
	?>

	<div id="content" class="site-content">
