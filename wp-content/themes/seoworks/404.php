<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package SeoWorks
 */

get_header();
$featured_img = get_field('service_custom_background_image');
$featured_default_img = get_template_directory_uri(). '/assets/images/SeoWorks-Generic-Header.jpg' ;
?>

<div class="header-content divider-bottom-left" style="background-image:url('<?php if (empty($featured_img)) {echo $featured_default_img; } else { echo $featured_img;} ?>')">
	<div class="container">
		<div class="col-12">
			<h1>404</h1>
		</div>
	</div>
</div>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<section id="page-content error-404 not-found centered">
	      <div class="container">
	        <div class="row">
	          <div class="col-12">

							<header class="page-header">
								<h2 class="page-title"><?php esc_html_e( 'Oops! This page can&rsquo;t be found.', 'seoworks' ); ?></h2>
							</header><!-- .page-header -->

							<div class="page-content">
								<p><?php esc_html_e( 'It looks like nothing was found at this location.', 'seoworks' ); ?></p>
								<?php //get_search_form();?>
							</div>

						</div>
					</div>
				</div><!-- .page-content -->
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
