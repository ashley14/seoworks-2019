<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package SeoWorks
 */

get_header();
$term_id = get_queried_object()->id;
$term_name = get_queried_object()->name;
// $taxonomy = $term->taxonomy;
$term_description = get_queried_object()->description;
$post_type = get_post_type();
?>

<div class="archive">
  <div class="header-content divider-bottom-left" style="background-image: url(<?php the_field('reviews_background_image', 'option'); ?>);">
  	<div class="container">
  		<div class="row">
  			<div class="col-12">
  				<div class="title">
              <h1><?php the_field('reviews_title', 'option'); ?></h1>
  				</div>
  				<p>
            <?php the_field('reviews_description', 'option'); ?>
  				</p>
  			</div>
  		</div>
  	</div>
  </div>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">


      <section>
  			<div class="sections">
  				<div class="container">
  					<div class="row">
  						<?php
              /* Start the Loop */
              if ( have_posts() ) :
                while ( have_posts() ) :
  				            the_post(); ?>

                <article class="col-sm-6 col-xl-6">
                  <div class="card">
                    <div class="card-image">
                      <figure>
                        <a href="<?php the_permalink(); ?>">
                          <?php the_post_thumbnail('full'); ?>
                        </a>
                      </figure>
                    </div>
                    <div class="card-body">
                      <h5 class="card-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                      <div class="card-text"><?php the_excerpt(); ?></div>
                      <a href="<?php the_permalink(); ?>" class="btn btn-readmore_blog">Read More</a>
                    </div>
                  </div>
                </article>

                <?php
                endwhile;
                //   the_posts_navigation();
                // else :
                //   get_template_part( 'template-parts/content', 'none' );
                endif;
                ?>

  					</div>
  				</div>
  			</div>



		</main><!-- #main -->
	</div><!-- #primary -->
</div>

<?php
// get_sidebar();
get_footer();
