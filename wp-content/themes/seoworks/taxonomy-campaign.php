<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package SeoWorks
 */

get_header();

$term_id = get_queried_object()->is;
$term_name = get_queried_object()->name;
// $taxonomy = $term->taxonomy;
$term_description = get_queried_object()->description;
$post_type = get_post_type();
// $taxonomy_names = get_post_taxonomies();
$featured_img = z_taxonomy_image_url(NULL, 'page-header');
$featured_default_img = get_template_directory_uri(). '/assets/images/SeoWorks-Generic-Header.jpg' ;?>

<div class="archive">
  <div class="header-content divider-bottom-left" style="background-image:url('<?php if (empty($featured_img)) {echo $featured_default_img; } else { echo $featured_img;} ?>')">
  	<div class="container">
  		<div class="row">
  			<div class="col-12">
  				<?php
  					// while ( have_posts() ) :
  					// 	the_post();
  				?>
  				<div class="title">
  					<?php if (is_tax() && is_archive()) { ?>
  						<h1><?php echo $term_name; ?></h1>
  					<?php } else { ?>
  						<h1><?php the_title(); ?></h1>
  					<?php } ?>
  				</div>

  				<p>
  					<?php if (is_tax()) {
  						 echo $term_description;
  					 } else {
  						 the_content();
  					 }
  					 ?>
  				</p>
  				<?php
  					// endwhile; // End of the loop.
  				?>
  			</div>
  		</div>
  	</div>
  </div>
  <div id="primary" class="content-area">
  	<main id="main" class="site-main">
  		<section>
  			<!-- <div class="sections"> -->

          <div class="container">
            <div class="row">
              <div class="col-12">
                <div class="home-sectors-portfolio_filter">
                  <?php
                  // your taxonomy name
                    $tax_campaign = 'campaign';
                    // get the terms of taxonomy
                    $terms_campaign = get_terms( $tax_campaign, $args = array(
                      'hide_empty' => true, // do not hide empty terms
                    ));
                  ?>

                  <div class="case-studies_filter case-studies_filter-sector">

                    <div class="filter">
                      <?php
                      echo '<a href="'. get_home_url() .'/search-engine-optimisation-testimonials/'.'">All</a>';
                        if ( ! empty( $terms_campaign ) && ! is_wp_error( $terms_campaign ) ){
                            // loop through all terms
                            foreach( $terms_campaign as $term ) {
                                // Get the term link
                                $term_link = get_term_link( $term );
                                if( $term->count > 0 )
                                   // display term name
                                  // echo '<button type="button" data-filter="' . '.' . $term->slug . '"> ' . $term->name . '</button>';
                                  echo '<a href="'. get_home_url() .'/campaign/'. $term->slug .'"> ' . $term->name . '</a>';
                            }
                        }
                        ?>
                    </div>
                  </div>

                </div>
              </div>
            </div>
          </div>

          <div class="container">
            <div class="row">
              <div class="col-12 ">
                <div class="owl-carousel carousel-case_studies text-center">
                  <?php
                    // get the current taxonomy term
                    $queried_object = get_queried_object();
                    $taxonomy = $queried_object->taxonomy;
                    $term_id = $queried_object->term_id;

                    // check if the repeater field has rows of data
                    if( have_rows('carousel_case_studies_items', 'term_' . $term_id) ):
                    // loop through the rows of data
                      while ( have_rows('carousel_case_studies_items', 'term_' . $term_id) ) : the_row();

                        if( have_rows('carousel_case_studies_item', 'term_' . $term_id) ):
                           while( have_rows('carousel_case_studies_item', 'term_' . $term_id) ): the_row();

                           $image = get_sub_field('carousel_case_studies_item_logo');
                           $link = get_sub_field('carousel_case_studies_item_link');
                  ?>
                  <div class="carousel-casestudies">
                    <?php if( get_sub_field('carousel_case_studies_item_link') ): ?>
                    <a target="_blank" href="<?php echo $link; ?>">
                    <?php endif; ?>
                      <figure>
                       <img src="<?php echo $image; ?>" alt="">
                      </figure>
                      <?php if( get_sub_field('carousel_case_studies_item_link') ): ?>
                    </a>
                    <?php endif; ?>
                  </div>

                  <?php
                          endwhile;
                        endif;
                      endwhile;
                    endif;
                    wp_reset_postdata();
                  ?>
                </div>
              </div>
            </div>
          </div>

  				<div class="container">
  					<div class="row">
  						<?php
  						// Query Arguments
  							$args = array(
  							'post_type' => $post_type,
  							'post_status' => array('publish'),
  							'tax_query' => array(
  				        // 'relation' => 'OR',
  				        array(
  				            'taxonomy' => 'campaign',
  				            'field'    => 'name',
  				            'terms'    => array($term_name)
  					        ),
  								),
  							'posts_per_page' => 9,
  							'order' => 'DESC',
  							'orderby' => 'date',
  							);

  							// The Query
  							$blog_post = new WP_Query( $args );

  							// The Loop
  							if ( $blog_post->have_posts() ) {
  							while ( $blog_post->have_posts() ) {
  								$blog_post->the_post(); ?>

  								<article class="col-sm-6 col-xl-4">
  									<div class="card">
  										<div class="card-image">
  											<figure>
  												<a href="<?php the_permalink(); ?>">
                            <?php if (class_exists('MultiPostThumbnails')) :
                              MultiPostThumbnails::the_post_thumbnail(
                                  get_post_type(),
                                  'second-image-casestudy',
                                  add_image_size('thumb-blog')
                              );
                            endif; ?>
  												</a>
  											</figure>
  										</div>
  										<div class="card-body">
  											<!-- <p class="card-date">Date posted: <span><?php // the_date(); ?></span></p> -->
  											<h5 class="card-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
  											<div class="card-text"><?php the_excerpt(); ?></div>
  											<!-- <a href="<?php //the_permalink(); ?>" class="btn btn-readmore_blog">Read More</a> -->
  										</div>
  									</div>
  								</article>

  						<?php  }
  							} else {
  							// no posts found
  							}
  							/* Restore original Post Data */
  							wp_reset_postdata();
  						?>

  					</div>
  				</div>
  			<!-- </div> -->
  		</section>
  	</main><!-- #main -->
  </div><!-- #primary -->
</div>



<?php
get_footer();
